
@extends('layouts.front_layout.front_design')

@section('content')
<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					@include('layouts.front_layout.account_left_bar')
				</div>
				<div class="col-sm-9 padding-right">
					@yield('account_section')
				</div>
			</div>
		</div>
	</section>
  @endsection