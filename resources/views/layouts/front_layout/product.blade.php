<div class="col-sm-4">
  <div class="product-image-wrapper">
    <div class="single-products">
        <div class="productinfo text-center">
          <img src="{{asset('images/product_images/'.$product->image)}}" alt="" />
          <h2>TK {{$product->price}}</h2>
          <p>{{$product->product_name}}</p>
          <a href="{{url('/product/'.$product->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
        </div>
        <div class="product-overlay">
          <div class="overlay-content">
            <h2>TK {{$product->price}}</h2>
            <p>{{$product->product_name}}</p>
            <a href="{{url('/product/'.$product->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
          </div>
        </div>
        @if( $product->created_at >= date('Y-m-d', strtotime('-7 days')))
        
          <img src="{{asset('images/front_images/home/new.png')}}" class="new" alt="">
        @endif
    </div>
    {{-- <div class="choose">
      <ul class="nav nav-pills nav-justified">
        <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
        <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
      </ul>
    </div> --}}
  </div>
</div>