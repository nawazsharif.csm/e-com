<?php
	$url=explode("/",$_SERVER['REQUEST_URI']);
	$url=isset($url[2])!=null ? $url[2] :'';
?>
<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a @if($url=='profile') class="nav-active" @endif href="{{url('/account/profile')}}">Profile</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a @if($url=='order') class="nav-active" @endif href="{{url('/account/order')}}">Orders</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					
						<div class="shipping text-center"><!--shipping-->
							<img src="{{asset('images/front_images/home/shipping.jpg')}}" alt="" />
						</div><!--/shipping-->
					
					</div>