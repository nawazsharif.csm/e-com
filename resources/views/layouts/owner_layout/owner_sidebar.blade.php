<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('/images/admin_images/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/images/admin_photos/'.Auth::guard('admin')->user()->image)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ ucfirst(Auth::guard('admin')->user()->name) }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @if(Session::get('page')=="dashboard")
               <?php $active ="active"; ?>
               @else
               <?php $active =""; ?>
               @endif
          <li class="nav-item has-treeview menu-open">
            <a href="{{route('owner.dashboard')}}" class="nav-link {{$active}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                {{-- <i class="right fas fa-angle-left"></i> --}}
              </p>
            </a>
          </li>
          @if(Session::get('page')=="settings" || Session::get('page')=='updateAdminDetails')
          <?php $active ="active"; $menuopen='menu-open'; ?>
          @else
          <?php $active =""; $menuopen=''; ?>
          @endif
          <li class="nav-item has-treeview {{$menuopen}}">
            <a href="#" class="nav-link {{$active}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Settings
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            
            <ul class="nav nav-treeview">
              @if(Session::get('page')=="settings")
              <?php $active ="active"; ?>
              @else
              <?php $active =""; ?>
              @endif
              <li class="nav-item">
                <a href="{{route('owner.settings')}}" class="nav-link {{$active}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update Owner Password</p>
                </a>
              </li>
              @if(Session::get('page')=="updateOwnerDetails")
              <?php $active ="active"; ?>
              @else
              <?php $active =""; ?>
              @endif
              <li class="nav-item">
                <a href="{{route('owner.details.update')}}" class="nav-link {{$active}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update Owner Details</p>
                </a>
              </li>
            </ul>
           
           @if(Session::get('page')=="products" || Session::get('page')=='inactiveProducts'|| Session::get('page')=='activeProducts')
            <?php $active ="active"; $menuopen='menu-open'; ?>
            @else
            <?php $active =""; $menuopen=''; ?>
            @endif
          <li class="nav-item has-treeview {{$menuopen}}">
            <a href="#" class="nav-link {{$active}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Products
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            
            <ul class="nav nav-treeview">
              @if(Session::get('page')=="products")
              <?php $active ="active";?>
              @else
              <?php $active =""; ?>
              @endif
              <li class="nav-item">
                <a href="{{url('/owner/products')}}" class="nav-link {{$active}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product List</p>
                </a>
              </li>
              @if(Session::get('page')=="activeProducts")
              <?php $active ="active";?>
              @else
              <?php $active =""; ?>
              @endif
              <li class="nav-item">
                <a href="{{url('/owner/active-products')}}" class="nav-link {{$active}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Products</p>
                </a>
              </li>
              @if(Session::get('page')=="inactiveProducts")
              <?php $active ="active";?>
              @else
              <?php $active =""; ?>
              @endif
              <li class="nav-item">
                <a href="{{url('/owner/inactive-products')}}" class="nav-link {{$active}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>In-active Products</p>
                </a>
              </li>
              
            </ul>
          </li>
          @if(Session::get('page')=="order")
            <?php $active ="active"; $menuopen='menu-open'; ?>
            @else
            <?php $active =""; $menuopen=''; ?>
            @endif
          <li class="nav-item has-treeview {{$menuopen}}">
            <a href="#" class="nav-link {{$active}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Orders
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          <ul class="nav nav-treeview">
              @if(Session::get('page')=="order")
              <?php $active ="active"; ?>
              @else
              <?php $active =""; ?>
              @endif
              <li class="nav-item">
                <a href="{{url('/owner/orders')}}" class="nav-link {{$active}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Orders List</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>