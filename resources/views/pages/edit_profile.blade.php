
@extends('layouts.front_layout.account_layout')
<style>
.padding-top-10 {
    padding-top: 10px !important;
}
.about-me .personal-info {
    padding: 25px;
    display: inline-block;
    width: 100%;
}
.about-me .personal-info li span {
    width: 50%;
    float: left;
    text-transform: uppercase;
    font-weight: bold;
}
.about-me .personal-info li {
    width: 50%;
    float: left;
}
.green{
	color:green;
}
.red{
	color:red;
}
</style>
@section('account_section')
<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Account Info</h2>
		
<section class="about-me padding-top-10"> 
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="login-form"><!--login form-->
						<h2>Account Info</h2>
						<form action="{{url('account/edit-profile')}}" method="post" enctype="multipart/form-data">
						@csrf
							<input type="text" name="name" placeholder="Name" value="{{Auth::user()->name}}" />
							<input type="email" name="email" placeholder="Email Address" value="{{Auth::user()->email}}" />
							<input type="text" name="mobile" placeholder="Mobile Number" value="{{Auth::user()->mobile}}" />
              <input type="text" name="address" placeholder="Address" value="{{Auth::user()->address}}" />
              <input type="text" name="state" placeholder="State" value="{{Auth::user()->state}}" />
              <input type="text" name="country" placeholder="Country" @if(!empty(Auth::user()->country)) value="{{Auth::user()->country}}" @else value="Bangladesh" @endif" />
              <input type="text" name="zip" placeholder="Zip" value="{{Auth::user()->zip}}" />
							<textarea name="" id="" cols="30" rows="10" name="bio" placeholder="About Yourself"></textarea>
              <input type="file" name="profile_image"/>
              @if(!empty(Auth::user()->image))
              <div>
              <input type="hidden" name="prev_img" value="{{Auth::user()->image}}">
              <img height="100" src="{{asset('/images/user_images/'. Auth::user()->image)}}" alt="">
              </div>
              @endif
							

      
							
							<button type="submit" class="btn btn-default">Update</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Change Your Password</h2>
						<form action="{{url('account/password-reseet')}}" method="post" >
						@csrf
							<input type="password" id="pass" name="password" placeholder="Current Password"/>
							<span id="errMsg" class="d-none"></span>
							<input type="password" name="new_pass" placeholder="New Password"/>
              <input type="password" name="re_pass" placeholder="Re-Enter Password"/>
							@if (Session::has('error_message'))
            		<span class="red">{{Session::get('error_message')}}</span>
        			@endif
							<button type="submit" class="btn btn-default">Update Password</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>   
</section>
	
</div>

@endsection
