
@extends('layouts.front_layout.account_layout')
<style>
.padding-top-10 {
    padding-top: 10px !important;
}
.about-me .personal-info {
    padding: 25px;
    display: inline-block;
    width: 100%;
}
.about-me .personal-info li span {
    width: 50%;
    float: left;
    text-transform: uppercase;
    font-weight: bold;
}
.about-me .personal-info li {
    width: 50%;
    float: left;
}
</style>
@section('account_section')
<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Account Info</h2>
		<div class="col-sm-10">
			@if (Session::has('success_message'))
					<div class="alert alert-success alert-dismissible show" role="alert">
						<strong>Success: </strong> {{Session::get('success_message')}}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
			@endif
			<section class="about-me padding-top-10"> 
                      
					<!-- Personal Info -->
					<ul class="personal-info">
						<li>
							<p> <span> Name</span> {{Auth::user()->name}} </p>
						</li>
						<li>
							<p> <span> Age</span> 38 Years </p>
						</li>
						<li>
							<p> <span> Location</span> Rome, Italy </p>
						</li>
						<li>
							<p> <span> Experience</span> 15 Years </p>
						</li>
						<li>
							<p> <span> Career Level</span> Mid-Level </p>
						</li>
						<li>
							<p> <span> Phone</span> {{Auth::user()->mobile}} </p>
						</li>
					
						<li>
							<p> <span> E-mail</span>  {{Auth::user()->email}} </p>
						</li>
						
					</ul>
					
					<!-- I’m Web Designer -->
					<h3 class="tittle">Personal Bio</h3>
					<div class="padding-20">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. <br>
							<br>
							Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci. Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu, vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor, et mollis pede metus eget nisi. <br>
							<br>
						</p>
					</div>
                      
                    
                      
				</section>
		</div>
		<div class="col-sm-2"><img height="100" style="margin-bottom:15px;" src="{{asset('/images/user_images/'. Auth::user()->image)}}" alt="">
		<a class="btn btn-success" href="{{url('account/edit-profile')}}">Edit Profile</a></div>
</div>

@endsection
