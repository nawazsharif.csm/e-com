@extends('layouts.front_layout.front_design')

@section('content')
@if(Auth::user()->status==0)
<div class="text-center" style="margin:100px">
<h2>You dont have right permission please contact with admin</h2></div>
@else
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
						</tr>
					</thead>
					<tbody>
				@foreach ($cart_lists as $cart)
						<?php
						$product=App\Product::where('id',$cart->product_id)->first(); 
						?>
            <tr>
							<td class="cart_product">
								<a href=""><img style="width:80px;" src="{{asset('images/product_images/'.$product->image)}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$cart->product_name}}</a></h4>
								<p>Code: {{$cart->product_code}}-{{$cart->size}}</p>
							</td>
							<td class="cart_price">
								<p>TK {{$cart->price}}</p>
							</td>
							<td class="cart_price">
									<p>{{$cart->quantity}}</p>
							</td>
							<td class="cart_total">
								<p id="cart_total_price_{{$cart->id}}" class="cart_total_price">TK {{$cart->price * $cart->quantity}}</p>
							</td>
							
						</tr>
                
            @endforeach
					
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>TK {{$total_price}}</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>TK 100</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>TK {{$total_price+100}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="step-one">
				<h2 class="heading">Shipping Info</h2>
			</div>
			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
				<form method="post" action="{{url('checkout')}}">
				@csrf
				<input type="hidden" name="total_price" value="{{$total_price+100}}">
					<div class="col-sm-6 clearfix">
						<div class="bill-to">
							<p>Bill Address</p>
							<div class="form-one">
								
								<input type="text" hidden name="user_id" placeholder="id" value="{{Auth::user()->id}}">
								<input type="text" readonly id="user_name" name="user_name" placeholder="Name" value="{{Auth::user()->name}}">
								<input type="text" readonly id="user_email" name="user_email" placeholder="Email*" value="{{Auth::user()->email}}">
								<input type="text" id="user_address" name="user_address" placeholder="Address" value="{{Auth::user()->address}}">
								<input type="text" readonly id="user_phone" name="user_phone" placeholder="Phone *" value="{{Auth::user()->mobile}}">
							
							</div>
							<div class="form-two">
								
								<select id="user_state" name="user_state" @if(!empty(Auth::user()->state)) value="{{Auth::user()->state}}" @endif>
										<option>-- State / Province / Region --</option>
										<option value="Dhaka">Dhaka</option>
										<option value="Rajshahi">Rajshahi</option>
										<option value="Barisal">Barisal</option>
										<option value="Khulna">Khulna</option>
										<option value="Rangpur">Rangpur</option>
										<option value="Chittagong">Chittagong</option>
										<option value="Sylhet">Sylhet</option>
										<option value="Mymensingh">Mymensingh</option>
									</select>
									<select id="user_country" name="user_country">
										<option>-- Country --</option>
										<option selected value="Bangladesh">Bangladesh</option>
									</select>
									<input type="text" id="user_zip" name="user_zip"  @if(!empty(Auth::user()->zip)) value="{{Auth::user()->zip}}" @endif placeholder="Zip / Postal Code *">
							
								<label><input type="checkbox" id="same_address" > Shipping to bill address</label>
							</div>
						</div>
						
					</div>
					<div class="col-sm-6 clearfix">
						<div class="bill-to">
							<p>Shipping Address</p>
							<div class="form-one">
							
									<input type="text" id="shipping_name" name="shipping_name" placeholder="Name">
									<input type="text" id="shipping_email" name="shipping_email" placeholder="Email*">
									<input type="text" id="shipping_address" name="shipping_address" placeholder="Address">
									<input type="text" id="shipping_phone" name="shipping_phone" placeholder="Phone *">
								
							</div>
							<div class="form-two">
								
								<select id="shipping_state" name="shipping_state">
										<option>-- State / Province / Region --</option>
										<option value="Dhaka">Dhaka</option>
										<option value="Rajshahi">Rajshahi</option>
										<option value="Barisal">Barisal</option>
										<option value="Khulna">Khulna</option>
										<option value="Rangpur">Rangpur</option>
										<option value="Chittagong">Chittagong</option>
										<option value="Sylhet">Sylhet</option>
										<option value="Mymensingh">Mymensingh</option>
									</select>
									
									<select id="shipping_country" name="shipping_country" >
										<option>-- Country --</option>
										<option>Bangladesh</option>
									</select>
									<input type="text" id="shipping_zip" name="shipping_zip" placeholder="Zip / Postal Code *">
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="order-message">
							<p>Shipping Order</p>
							<textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="6"></textarea>
						</div>	
					</div>	
					<div class="col-md-12" style="margin-bottom:50px;">
							<button typr="submit" class="btn btn-default" href="">Order Now</button>
					</div>
					</form>				
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->
@endif
@endsection