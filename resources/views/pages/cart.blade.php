	@extends('layouts.front_layout.front_design')

@section('content')
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			@guest()
			<div class="text-center">
			<h3>You need to loging first for checkout</h3></div>
			@endguest

			<div class="table-responsive cart_info">

				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					
					<tbody>
						@foreach ($cart_lists as $cart)
						<?php
						$product=App\Product::where('id',$cart->product_id)->first(); 
						?>
            <tr>
							<td class="cart_product">
								<a href=""><img style="width:80px;" src="{{asset('images/product_images/'.$product->image)}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$cart->product_name}}</a></h4>
								<p>Code: {{$cart->product_code}}-{{$cart->size}}</p>
							</td>
							<td class="cart_price">
								<p>TK {{$cart->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" id="cart_quantity_up_{{$cart->id}}" data-id="{{$cart->id}}" data-product_id="{{$cart->product_id}}" data-size="{{$cart->size}}" href=""> + </a>
									<input class="cart_quantity_input" readonly="" id="cart_{{$cart->id}}" type="text" name="quantity" value="{{$cart->quantity}}" autocomplete="off" size="2">
									<a style="@if($cart->quantity < 2) display:none; @endif" id="cart_quantity_down_{{$cart->id}}" data-product_id="{{$cart->product_id}}" data-size="{{$cart->size}}" class="cart_quantity_down" data-id="{{$cart->id}}" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p id="cart_total_price_{{$cart->id}}" class="cart_total_price">TK {{$cart->price * $cart->quantity}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{url('cart/'.$cart->id)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                
            @endforeach
					</tbody>
				</table>
				
			</div>
			@if ($cart_lists->count()==0)
            <div class="alert alert-success alert-dismissible show" role="alert">
              Your Card list Empty Please add some product First <strong><a href="{{url('/')}}"> Click Here </a> </strong> To add Product
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
		
			<div class="row">
				<div class="col-sm-6">
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>TK {{$total_price}}</span></li>
							<li>Shipping Cost <span>TK 100</span></li>
							<li>Total <span id="total_price">TK {{$total_price+100}}</span></li>
						</ul>
							{{-- <a class="btn btn-default update" href="">Update</a> --}}
							@if(Auth::check())
							<a class="btn btn-default check_out" @if($cart_lists->count()==0 || Auth::user()->status == 0) disabled="" @endif href="{{url('/checkout')}}">Check Out</a>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
  @endsection