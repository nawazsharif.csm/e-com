
@extends('layouts.front_layout.front_design')

@section('content')
<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>Free E-Commerce Template</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="{{asset('images/front_images/home/girl1.jpg')}}" class="girl img-responsive" alt="" />
									<img src="{{asset('images/front_images/home/pricing.png')}}"  class="pricing" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>100% Responsive Design</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="{{asset('images/front_images/home/girl2.jpg')}}" class="girl img-responsive" alt="" />
									<img src="{{asset('images/front_images/home/pricing.png')}}"  class="pricing" alt="" />
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>Free Ecommerce Template</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="{{asset('images/front_images/home/girl3.jpg')}}" class="girl img-responsive" alt="" />
									<img src="{{asset('images/front_images/home/pricing.png')}}" class="pricing" alt="" />
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					@include('layouts.front_layout.left_category')
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">{{$categoryDetails->category_name}}</h2>
            @foreach ($productsAll as $product)
								@include('layouts.front_layout.product')
						@endforeach
					</div><!--features_items-->
					
					<div class="category-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
							<?php $count=0; ?>
							@foreach ($categories as $category)
									<li @if($count==0)class="active"@endif><a href="#{{$category['url']}}" data-toggle="tab">{{$category['category_name']}}</a></li>
							<?php $count++; ?>
							@endforeach
								
								{{-- <li><a href="#blazers" data-toggle="tab">Blazers</a></li>
								<li><a href="#sunglass" data-toggle="tab">Sunglass</a></li>
								<li><a href="#kids" data-toggle="tab">Kids</a></li>
								<li><a href="#poloshirt" data-toggle="tab">Polo shirt</a></li> --}}
							</ul>
						</div>
						<div class="tab-content">
						@foreach ($categories as $category)
							<div class="tab-pane fade active in" id="{{$category['url']}}" >
							<?php 
							$url=$category['url'];
							$categoryDetails = App\Category::where(['url' => $url])->first();
							//print_r('<pre>');
							//var_dump(json_decode(json_encode($categoryDetails)));
							if ($categoryDetails->parent_id == 0) {
									$cat_ids = [$categoryDetails['id']];
									$subCategories = App\Category::where(['parent_id' => $categoryDetails->id])->get();
									foreach ($subCategories as $subCategory) {
										$category=json_decode(json_encode($subCategory),true);
											array_push($cat_ids, $category['id']);
									}
									//var_dump($cat_ids);

									$products = App\Product::whereIn('category_id', $cat_ids)->where(['status'=>1])->inRandomOrder()->limit(4)->get();
							} else {
									$products = App\Product::where(['category_id' => $categoryDetails['id']])->where(['status'=>1])->inRandomOrder()->limit(4)->get();
							}
							//var_dump($products);
							?>
								@foreach ($products as $product)
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('images/product_images/'.$product->image)}}" alt="" />
												<h2>TK {{$product->price}}</h2>
												<p>{{$product->product_name}}</p>
												<a href="{{url('/product/'.$product->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								@endforeach
								
							</div>
							@endforeach
						</div>
					</div><!--/category-tab-->
					
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">Hot Items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<?php $count=1?>
							@foreach ($hotProducts->chunk(3) as $products)
								<div <?php if($count==1){?> class="item active"<?php } else{?> class="item" <?php } ?>	>
								
								@foreach ($products as $product)
										<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('images/product_images/'.$product->image)}}" alt="" />
													<h2>TK {{$product->price}}</h2>
													<p>{{$product->product_name}}</p>
													<a href="{{url('/product/'.$product->id)}}" type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
									</div>
								@endforeach
								<?php $count++?>
								</div>
								@endforeach
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</>
					</div><!--/recommended_items-->
					
				</div>
			</div>
		</div>
	</section>
  @endsection