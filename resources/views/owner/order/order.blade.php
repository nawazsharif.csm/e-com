@extends('layouts.owner_layout.owner_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Orders</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Orders</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <div class="col-12">
          @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
          <div class="card">
            <div class="card-header">
              <h3 class="card-title mt-2">Orders</h3>
           </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="orders" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Order Id</th>
                  <th>Amoount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                @foreach ($orders as $order)
                
                  <tr>
                  <td>{{$count++}}</td>
                  <td>{{$order->session_id}}</td>
                  <td>{{$order->totalAmount}}</td>
                  {{-- <td>{{$order->items->count()}}</td> --}}
                  {{-- <td>{{$order->status}}</td> --}}
                  <td>
                    <ul class="list-inline m-0">
                      <li class="list-inline-item">
                          <a href="{{url('/owner/orders/'.$order->session_id)}}" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-eye"></i></a>
                      </li>
                      {{-- <li class="list-inline-item">
                          <a href="{{url('/admin/delete-banner/'.$order->id)}}" class="confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                      </li> --}}
                    </ul>
                </td>
                </tr>
                @endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection