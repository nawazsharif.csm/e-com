@extends('layouts.owner_layout.owner_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Orders</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active"><a href="/owner/orders">Orders</a></li>
              <li class="breadcrumb-item active">Order List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Info </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>Name </td>
                  <td>{{$order_detail->shipping_name}}</td>
                </tr>
                <tr>
                  <td>Email </td>
                  <td>{{$order_detail->shipping_email}}</td>
                </tr>
                <tr>
                  <td>Phone </td>
                  <td>{{$order_detail->shipping_phone}}</td>
                </tr>
                <tr>
                  <td>Total Product </td>
                  <td>{{$order_list->count()}}</td>
                </tr>
                <tr>
                  <td>Total amount </td>
                  <td>{{$total_amount[0]->totalAmount}}</td>
                </tr>
                <tr>
                  <td>Order Date </td>
                  <td>{{$orders->created_at}}</td>
                </tr>

                
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-6">
      <div class="card">
          <div class="card-header">
            <h3 class="card-title"> Shipping address </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>Address </td>
                  <td>{{$order_detail->shipping_address}}</td>
                </tr>
                <tr>
                  <td>State </td>
                  <td>{{$order_detail->shipping_state}}</td>
                </tr>
                <tr>
                  <td>Country</td>
                  <td>{{$order_detail->shipping_country}}</td>
                </tr>
                <tr>
                  <td>Zip</td>
                  <td>{{$order_detail->shipping_zip}}</td>
                </tr>

                
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      
        <div class="col-12">
          @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
          <div class="card">
            <div class="card-header">
              <h3 class="card-title mt-2">Order List</h3>
           </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="orders" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Color</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>price</th>
                  <th>status</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($order_list as $order)
                <tr>
                  <td>{{$order->product_name}}</td>
                  <td>{{$order->product_code}}</td>
                  <td>{{$order->product_color}}</td>
                  <td>{{$order->size}}</td>
                  <td>{{$order->quantity}}</td>
                  <td>{{$order->price}}</td>
                  <td>
                    <select name="" data-id="{{$order->id}}" class="order_status">
                      <option  value="">select from below</option>
                      <option @if($order->status==1){{__('selected')}}@endif value="1">New</option>
                      <option @if($order->status==2){{__('selected')}}@endif value="2">Panding</option>
                      <option @if($order->status==3){{__('selected')}}@endif value="3">Shipping</option>
                      <option @if($order->status==4){{__('selected')}}@endif value="4">Close</option>
                    </select>
                  </td>
                </tr>
                @endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection