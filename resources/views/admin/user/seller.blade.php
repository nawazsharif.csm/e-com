@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Seller</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">seller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <div class="col-12">
          @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
          <div class="card">
            <div class="card-header">
              <h3 class="card-title mt-2">Seller List</h3>
            {{-- <a class="btn btn-nlock btn-success float-sm-right" href="{{url('/admin/add-edit-owner')}}">Add owner</a> --}}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="categories" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($owners as $owner)
                
                  <tr>
                  <td>{{$owner->id}}</td>
                  <td>{{$owner->name}}</td>
                  <td><a href="{{url('/images/admin_photos/'.$owner->image)}}" target="_blank"><img style="height:50px" src="{{asset('/images/owner_images/'.$owner->image)}}" alt=""></a></td>
                  <td>
                  @if($owner->status==1)<a class="updateSellerStatus" href="javascript:void(0)" id="seller_{{$owner->id}}" seller_id="{{$owner->id}}">Active</a>@else<a class="updateSellerStatus" href="javascript:void(0)" id="seller_{{$owner->id}}" seller_id="{{$owner->id}}">Inactive</a>@endif
                  </td>
                  
                </tr>
                @endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection