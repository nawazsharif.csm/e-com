@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if ($errors->any())
          <div class="alert alert-danger">
              <ul class="mb-0">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        @if (Session::has('flash_success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('flash_success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <div class="col-md-6">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Product Info</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td>Product Name </td>
                      <td>{{$productData['product_name']}}</td>
                    </tr>
                    <tr>
                      <td>Product Code </td>
                      <td>{{$productData['product_code']}}</td>
                    </tr>
                    <tr>
                      <td>Product Color </td>
                      <td>{{$productData['product_color']}}</td>
                    </tr>

                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        
        <div class="col-md-12">
        <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <form action="{{url('/admin/add-attributes/'.$productData['id'])}}" method="post">
                @csrf
                  <table class="table table-head-fixed text-nowrap">
                    <tbody id="dynamicTable">
                      <tr>
                        <td><input type="text" class="form-control" name="product_sku[]" id="product_sku" placeholder="SKU"></td>
                        <td><input type="text" class="form-control" name="product_size[]" id="product_size" placeholder="Size"></td>
                        <td><input type="text" class="form-control" name="product_price[]" id="product_price" placeholder="Price"></td>
                        <td><input type="text" class="form-control" name="product_stock[]" id="product_stock" placeholder="Stock"></td>
                        <td>
                          <a href="javascript:void(0);" class="add_button btn btn-success btn-sm rounded-0"><i class="fa fa-plus"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="card-header">
                    <button type="submit" class=" btn btn-primary">Add Attributes</button>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
      </div>
        <!-- /.container-fluid -->
      @if(!empty($productData['attributes']))
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Attributes List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="attribute" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SKU</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($productData['attributes'] as $attribute)
                        <tr>
                          <td>{{$attribute['sku']}}</td>
                          <td>{{$attribute['size']}}</td>
                          <td>{{$attribute['price']}}</td>
                          <td>{{$attribute['stock']}}</td>
                          <td>
                            <a href="{{url('/admin/delete-attribute/'.$attribute['id'])}}" class="btn btn-danger btn-sm rounded-0"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                          
                      @endforeach
               
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
          </div> 
          </div>
       @endif
       </div>
    </section>
    <!-- /.content -->
    </div>
    


@endsection