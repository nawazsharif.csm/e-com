<div class="form-group">
  <label>Select Category Lavel</label>
  <select class="form-control select2" name="parent_id" id="parent_id" style="width: 100%;">   
    @if(!empty($getCategories))
      @foreach ($getCategories as $category)
      <option value="{{$category['id']}}" @if(isset($productData['category_id']) && $productData['category_id']==$category['id']) selected="" @endif >{{$category['category_name']}}</option>
        @if(!empty($category['subcategories']))
          @foreach($category['subcategories'] as $subcategory)
            <option value="{{$subcategory['id']}}" 
            @if(isset($productData['category_id']) && $productData['category_id']==$subcategory['id'])selected="" @endif
            >&nbsp;&raquo;&nbsp;{{$subcategory['category_name']}}</option>
          @endforeach
        @endif    
      @endforeach
      @else
      <option value="">No Category Found</option>
    @endif

  </select>
</div>