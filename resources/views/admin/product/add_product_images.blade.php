@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if ($errors->any())
          <div class="alert alert-danger">
              <ul class="mb-0">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        @if (Session::has('flash_success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('flash_success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <div class="col-md-6">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Product Info</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td>Product Name </td>
                      <td>{{$productData['product_name']}}</td>
                    </tr>
                    <tr>
                      <td>Product Code </td>
                      <td>{{$productData['product_code']}}</td>
                    </tr>
                    <tr>
                      <td>Product Color </td>
                      <td>{{$productData['product_color']}}</td>
                    </tr>

                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        
        <div class="col-md-6">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add Product images</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form action="{{url('/admin/add-images/'.$productData['id'])}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="text" hidden name="product_id" value="{{$productData['id']}}">
                 <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="product_image[]" id="product_image" multiple="multiple"  accept="image/*">
                      <label class="custom-file-label" for="product_image">Choose file</label>
                    </div>
                    <div class="input-group-append">
                      <button type="submit" class="input-group-text" id="">Upload</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
      </div>
        <!-- /.container-fluid -->
      @if(!empty($productData['images']))
        <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Product Image List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="attribute" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Image</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($productData['images'] as $productImage)
                      <tr>
                        <td>{{$productImage['id']}}</td>
                        <td>{{$productImage['image']}}</td>
                        <td>
                          <a href="{{url('/admin/delete-image/'.$productImage['id'])}}" class="btn btn-danger btn-sm rounded-0"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div> 
          </div>
       @endif
       </div>
    </section>
    <!-- /.content -->
    </div>
    


@endsection