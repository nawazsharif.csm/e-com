@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if ($errors->any())
          <div class="alert alert-danger">
              <ul class="mb-0">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        @if (Session::has('flash_success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('flash_success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        
      <form name="productForm" id="productForm" @if(!empty($productData['id'])) action="{{url('/admin/add-edit-product/'.$productData['id'])}}" @else action="{{url('/admin/add-edit-product')}}" @endif  method="post" enctype="multipart/form-data">
      @csrf
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">{{$title}}</h3>

            {{-- <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div> --}}
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Product Name</label>
                  <input type="text" class="form-control" name="product_name" id="product_name" @if(!empty($productData['product_name'])) value="{{$productData['product_name']}}" @else value="{{old('product_name')}}" @endif placeholder="Enter Product Name">
                </div>
                <div class="form-group">
                  <label>Select Section</label>
                  <select class="form-control select2" name="section_id" id="section_id" style="width: 100%;">
                    <option value="">Select</option>
                    @foreach ($sections as $section)
                        <option value={{$section->id}} @if(!empty($productData['section_id']) && $productData['section_id']==$section->id) selected @endif>{{$section->name}}</option>
                    @endforeach
                  </select>
                </div>
                
                <div class="form-group">
                  <label>Product Price</label>
                  <input type="text" class="form-control" name="product_price" id="product_price" @if(!empty($productData['price'])) value="{{$productData['price']}}" @else value="{{old('price')}}" @endif placeholder="Enter Product Price">
                </div>
                <div class="form-group">
                    <label>Product Description</label>
                    <textarea class="form-control" rows="3" name="description" id="description"  placeholder="Enter ...">@if(!empty($productData['description'])) {{$productData['description']}} @else {{old('description')}} @endif</textarea>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" name="isHot" @if(!empty($productData['isHot']) && $productData['isHot']==1)checked="checked"@endif class="form-check-input" value="1">
                    <label class="form-check-label"p>Hot Product</label>
                  </div>
                
                  
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Product Code</label>
                  <input type="text" class="form-control" name="product_code" id="product_code" @if(!empty($productData['product_code'])) value="{{$productData['product_code']}}" @else value="{{old('product_code')}}" @endif placeholder="Enter Product Code">
                </div>
                <div id="appendCategoriesLevel">
                  @include('admin.product.append_categories_level')
                </div>
                
                    {{-- @if(!empty($productData['category_image']))
                    <div>
                    <img src="{{url('/images/category_images/'.$productData['category_image'])}}" alt="Category Image" style="width: 80px;margin-top: 5px;">
                          <a href="{{url('/admin/delete-category-image/'.$productData['id'])}}" class="confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    </div>
                    @endif --}}
                <div class="form-group">
                  <label>product Color</label>
                  <input type="text" class="form-control" name="product_color" id="product_color" @if(!empty($productData['product_color'])) value="{{$productData['product_color']}}" @else value="{{old('product_color')}}" @endif placeholder="Enter Product Color">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Product Image</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="product_image" id="product_image" accept="image/*">
                      <label class="custom-file-label" for="product_image">Choose file</label>
                    </div>
                    <div class="input-group-append">
                      <span class="input-group-text" id="">Upload</span>
                    </div>
                  </div>
                  @if(!empty($productData['image']))
                  <div>
                    <img src="{{url('/images/product_images/'.$productData['image'])}}" alt="Product Image" style="width: 80px;margin-top: 5px;">
                          <a href="{{url('/admin/delete-product-image/'.$productData['id'])}}" class="confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                  </div>
                  @endif
                </div>
                @if(Auth::guard('admin')->user()->type=='admin')

                <div class="form-group">
                  <label>Select Owner</label>
                  <select class="form-control select2" name="owner_id" id="owner_id" style="width: 100%;">
                    <option value="">Select</option>
                    @foreach ($owners as $owner)
                        <option value="{{$owner->id}}" @if(!empty($owner->id) && $owner->id==$productData['product_owner']) selected @endif>{{$owner->name}}</option>
                    @endforeach
                  </select>
                </div>
                @endif

                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
              
      </form>
      </div><!-- /.container-fluid -->
      
    </section>
    <!-- /.content -->
    </div>

@endsection