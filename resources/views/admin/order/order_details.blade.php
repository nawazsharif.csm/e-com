@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Orders</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active"><a href="/orders">Orders</a></li>
              <li class="breadcrumb-item active">Order List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <div class="col-12">
          @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
          <div class="card">
            <div class="card-header">
              <h3 class="card-title mt-2">Order List</h3>
           </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="orders" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Color</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>price</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($order_list as $order)
                <tr>
                  <td>{{$order->product_name}}</td>
                  <td>{{$order->product_code}}</td>
                  <td>{{$order->product_color}}</td>
                  <td>{{$order->size}}</td>
                  <td>{{$order->quantity}}</td>
                  <td>{{$order->price}}</td>
                </tr>
                @endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection