@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{$title}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Banner</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              @if (Session::has('error_message'))
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error!! </strong> {{Session::get('error_message')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
              @endif
               @if (Session::has('success_message'))
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success: </strong> {{Session::get('success_message')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
              @endif

              <form role="form" method="post" @if(!empty($bannerData->id)) action="{{url('/admin/add-edit-banner/'.$bannerData->id)}}" @else action="{{url('/admin/add-edit-banner')}}" @endif enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                <div class="form-group">
                    <label>Banner Image</label>
                    
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="image" accept="image/*">
                        <label class="custom-file-label">Choose file</label>
                      </div>
                    </div>
                    @if(!empty($bannerData->image))
                    <input type="text" hidden name="prev_img" value="{{$bannerData->image}}">
                    <div>
                    <img src="{{url('/images/banner_images/'.$bannerData->image)}}" alt="Banner Image" style="width: 80px;margin-top: 5px;">
                          <a href="{{url('/admin/delete-banner-image/'.$bannerData->id)}}" class="confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    </div>
                    @endif
                   
                  </div>
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="@if(!empty($bannerData->title)){{$bannerData->title}}@else{{old('title')}}@endif" placeholder="Banner Title">
                  </div>
                  <div class="form-group">
                    <label for="type">Link</label>
                    <input type="text" class="form-control" name="link" value="@if(!empty($bannerData->title)){{$bannerData->link}}@else{{old('link')}}@endif" placeholder="Banner Link">
                  </div>
                  <div class="form-group d-flex">
                    <label>Status</label>
                    <input @if(!empty($bannerData->status)==1) checked="checked" @endif style="width: auto;margin: 2px 2px 2px 15px;padding: 0;height: 22px;" type="checkbox" value="1" class="form-control" name="status">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>

@endsection