@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Catalogues</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Catalogues</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if ($errors->any())
          <div class="alert alert-danger">
              <ul class="mb-0">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        @if (Session::has('flash_success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('flash_success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        
      <form name="CategoryForm" id="CategoryForm" @if(!empty($categorydata['id'])) action="{{url('/admin/add-edit-category/'.$categorydata['id'])}}" @else action="{{url('/admin/add-edit-category')}}" @endif  method="post" enctype="multipart/form-data">
      @csrf
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">{{$title}}</h3>

            {{-- <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div> --}}
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control" name="category_name" id="category_name" @if(!empty($categorydata['category_name'])) value="{{$categorydata['category_name']}}" @else value="{{old('category_name')}}" @endif placeholder="Enter Category Name">
                </div>
                <div id="appendCategoriesLevel">
                  @include('admin.category.append_categories_level')
                </div>
                <div class="form-group">
                  <label>Category Discount</label>
                  <input type="text" class="form-control" name="category_discount" id="category_discount" @if(!empty($categorydata['category_discount'])) value="{{$categorydata['category_discount']}}" @else value="{{old('category_discount')}}" @endif placeholder="Enter Category Discount">
                </div>
                <div class="form-group">
                    <label>Category Description</label>
                    <textarea class="form-control" rows="3" name="description" id="description"  placeholder="Enter ...">@if(!empty($categorydata['description'])) {{$categorydata['description']}} @else {{old('description')}} @endif</textarea>
                  </div>
                  <div class="form-group">
                    <label>Meta Description</label>
                    <textarea class="form-control" name="meta_description" id="meta_description" rows="3" placeholder="Enter ...">@if(!empty($categorydata['meta_description'])) {{$categorydata['meta_description']}} @else {{old('meta_description')}} @endif</textarea>
                  </div>
                
            
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Select Section</label>
                  <select class="form-control select2" name="section_id" id="section_id" style="width: 100%;">
                    <option value="">Select</option>
                    @foreach ($sections as $section)
                        <option value={{$section->id}} @if(!empty($categorydata['section_id']) && $categorydata['section_id']==$section->id) selected @endif>{{$section->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Category Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="category_image" id="category_image" accept="image/*">
                        <label class="custom-file-label" for="profile_image">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                    @if(!empty($categorydata['category_image']))
                    <div>
                    <img src="{{url('/images/category_images/'.$categorydata['category_image'])}}" alt="Category Image" style="width: 80px;margin-top: 5px;">
                          <a href="{{url('/admin/delete-category-image/'.$categorydata['id'])}}" class="confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    </div>
                    @endif
                   
                  </div>
                  <div class="form-group">
                  <label>Category URL</label>
                  <input type="text" class="form-control" name="url" id="url" @if(!empty($categorydata['url'])) value="{{$categorydata['url']}}" @else value="{{old('url')}}" @endif placeholder="Enter Category URL">
                </div>
                  <div class="form-group">
                    <label>Meta Title</label>
                    <textarea class="form-control" rows="3" name="meta_title" id="meta_title" placeholder="Enter ...">@if(!empty($categorydata['meta_title'])) {{$categorydata['meta_title']}} @else {{old('meta_title')}} @endif</textarea>
                  </div>
                  <div class="form-group">
                    <label>Meta Keywords</label>
                    <textarea class="form-control" rows="3" name="meta_keyword" id="meta_keyword" placeholder="Enter ...">@if(!empty($categorydata['meta_keyword'])) {{$categorydata['meta_keyword']}} @else {{old('meta_keyword')}} @endif</textarea>
                  </div>

                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
              
      </form>
      </div><!-- /.container-fluid -->
      
    </section>
    <!-- /.content -->
    </div>

@endsection