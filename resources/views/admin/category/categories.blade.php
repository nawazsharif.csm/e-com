@extends('layouts.admin_layout.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Categories</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Categories</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <div class="col-12">
          @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Success: </strong> {{Session::get('success_message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
          <div class="card">
            <div class="card-header">
              <h3 class="card-title mt-2">Categories</h3>
            <a class="btn btn-nlock btn-success float-sm-right" href="{{url('/admin/add-edit-category')}}">Add Category</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="categories" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>ParentCategory</th>
                  <th>section</th>
                  <th>URL</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($categories as $category)
                @if(!isset($category->parentCategory->category_name))
                
                <?php $parent_category='Root'; ?>
                @else
                <?php $parent_category=$category->parentCategory->category_name; ?>
                @endif
                    <tr>
                  <td>{{$category->id}}</td>
                  <td>{{$category->category_name}}</td>
                  <td>{{$parent_category}}</td>
                  <td>{{$category->section->name}}</td>
                  <td>{{$category->url}}</td>
                  <td>@if($category->status==1)
                  <a class="updateCategoryStatus" href="javascript:void(0)" id="category-{{$category->id}}" category_id="{{$category->id}}">Active</a>
                  @else
                  <a class="updateCategoryStatus" href="javascript:void(0)" id="category-{{$category->id}}" category_id="{{$category->id}}">Inactive</a>
                  @endif</td>
                  <td>
                    <ul class="list-inline m-0">
                      <li class="list-inline-item">
                          <a href="{{url('/admin/add-edit-category/'.$category->id)}}" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                      </li>
                      <li class="list-inline-item">
                          <a href="{{url('/admin/delete-category/'.$category->id)}}" class="confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                </td>
                </tr>
                @endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection