@component('mail::message')
# Welcome to SellsmanBD Start your carear from

The body of your message.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
