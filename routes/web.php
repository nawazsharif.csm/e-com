<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


Route::get('mail/queue', function () {
    $details['email'] = 'nawazsharif.csm@gmail.com';

    dispatch(new App\Jobs\SendEmailJob($details));

    dd('done');
});

Auth::routes();
Route::get('/', 'IndexController@index');
Route::get('products/{url}', 'IndexController@showProducts');
Route::get('product/{id}', 'IndexController@productsDetails');
Route::get('get-product-price', 'IndexController@getProductPrice');

// Route::get('/home', 'HomeController@index')->name('home');
Route::match(['get', 'post'], 'add-cart', 'IndexController@addCart');
Route::match(['get', 'post'], 'cart', 'IndexController@cart');
Route::get('cart/{id}', 'IndexController@deleteCart');
Route::post('update-cart', 'IndexController@updateCart');

//login route
Route::match(['get', 'post'], 'register', 'UsersController@Register');
Route::match(['get', 'post'], 'user-login', 'UsersController@login')->name('user.login');
Route::get('logout', 'UsersController@logout')->name('user.logout');


Route::group(['middleware' => ['frontLogin']], function () {
    // saler Route
    Route::get('account/profile', 'IndexController@account');
    Route::post('account/check-pass', 'IndexController@checkPass');
    Route::post('account/password-reseet', 'IndexController@resetPass');
    Route::match(['get', 'post'], 'account/edit-profile', 'IndexController@editProfile');
    Route::match(['get', 'post'], 'checkout', 'IndexController@checkout');
    Route::get('account/order', 'IndexController@allOrders');
    Route::post('order/change-status', 'HomeController@orderStatus');
});

Route::prefix('/owner')->namespace('Owner')->group(function () {
    Route::group(['middleware' => ['owner']], function () {
        // Route::match(['get', 'post'], '/', 'OwnerController@dashboard');
        Route::get('dashboard', 'OwnerController@dashboard')->name('owner.dashboard');
        Route::get('logout', 'OwnerController@logout')->name('owner.logout');
        Route::get('settings', 'OwnerController@settings')->name('owner.settings');
        Route::post('check-current-pwd', 'OwnerController@chkCurrentPassword')->name('owner.password.check');
        Route::post('update-current-pwd', 'OwnerController@updateCurrentPassword')->name('owner.password.update');
        Route::match(['get', 'post'], 'update-admin-details', 'OwnerController@updateOwnerDetails')->name('owner.details.update');
        // Products Route
        Route::get('products', 'ProductController@products');
        Route::match(['get', 'post'], 'add-edit-product/{id?}', 'ProductController@addEditProduct');
        Route::get('delete-product/{id}', 'ProductController@deleteProduct');
        Route::get('delete-product-image/{id}', 'ProductController@deleteProductImage');
        Route::get('active-products', 'ProductController@activeProducts');
        Route::get('inactive-products', 'ProductController@inactiveProducts');

        // Product Alternate Images
        Route::match(['get', 'post'], 'add-images/{id}', 'ProductController@addProductImages');
        Route::get('delete-image/{id}', 'ProductController@deleteImage');
        //Product attribute
        Route::match(['get', 'post'], 'add-attributes/{id}', 'ProductController@addAttributes');
        Route::get('delete-attribute/{id}', 'ProductController@deleteAttribute');

        Route::post('append-categories-level', 'CategoryController@appendCategoriesLevel');

        //Orders
        Route::match(['get', 'post'], 'orders', 'OwnerController@orders');
        Route::get('orders/{id}', 'OwnerController@orderDetails');
    });
});

Route::prefix('/admin')->namespace('Admin')->group(function () {
    //All the admin route define here

    Route::match(['get', 'post'], '/', 'AdminController@login')->name('admin.login');
    Route::group(['middleware' => ['admin']], function () {
        Route::get('dashboard', 'AdminController@dashboard')->name('admin.dashboard');
        Route::get('logout', 'AdminController@logout')->name('admin.logout');
        Route::get('settings', 'AdminController@settings')->name('admin.settings');
        Route::post('check-current-pwd', 'AdminController@chkCurrentPassword')->name('admin.password.check');
        Route::post('update-current-pwd', 'AdminController@updateCurrentPassword')->name('admin.password.update');
        Route::match(['get', 'post'], 'update-admin-details', 'AdminController@updateAdminDetails')->name('admin.details.update');

        // Section
        Route::get('sections', 'SectionController@sections')->name('admin.section');
        Route::post('update-section-status', 'SectionController@updateSectionStatus');
        // Categories
        Route::get('categories', 'CategoryController@categories');
        Route::post('update-category-status', 'CategoryController@updateCategoryStatus');
        Route::match(['get', 'post'], 'add-edit-category/{id?}', 'CategoryController@addEditCategory');
        Route::post('append-categories-level', 'CategoryController@appendCategoriesLevel');
        Route::get('delete-category/{id}', 'CategoryController@deleteCategory');
        Route::get('delete-category-image/{id}', 'CategoryController@deleteCategoryImage');
        // Products Route
        Route::get('products', 'ProductController@products');
        Route::match(['get', 'post'], 'add-edit-product/{id?}', 'ProductController@addEditProduct');
        Route::get('delete-product/{id}', 'ProductController@deleteProduct');
        Route::get('delete-product-image/{id}', 'ProductController@deleteProductImage');
        Route::get('active-products', 'ProductController@activeProducts');
        Route::get('inactive-products', 'ProductController@inactiveProducts');
        Route::post('update-product-status', 'ProductController@updateProductStatus');

        // Product Alternate Images
        Route::match(['get', 'post'], 'add-images/{id}', 'ProductController@addProductImages');
        Route::get('delete-image/{id}', 'ProductController@deleteImage');
        //Product attribute
        Route::match(['get', 'post'], 'add-attributes/{id}', 'ProductController@addAttributes');
        Route::get('delete-attribute/{id}', 'ProductController@deleteAttribute');

        //Banner Route
        Route::match(['get', 'post'], '/add-edit-banner/{id?}', 'BannersController@addBanner');
        Route::get('banners', 'BannersController@bannerList');
        Route::get('delete-banner/{id}', 'BannersController@deleteBanner');
        Route::get('delete-banner-image/{id}', 'BannersController@deleteBannerImage');
        //Orders
        Route::match(['get', 'post'], 'orders', 'AdminController@orders');
        Route::get('orders/{id}', 'AdminController@orderDetails');

        // Users
        Route::match(['get', 'post'], '/all-owners', 'AdminController@allOwners');
        Route::match(['get', 'post'], '/inactive-owners', 'AdminController@inActiveOwners');
        Route::match(['get', 'post'], '/all-sellers', 'AdminController@allSellers');
        Route::match(['get', 'post'], '/inactive-sellers', 'AdminController@inActiveSellers');

        // owner
        Route::post('update-owner-status', 'AdminController@updateOwnerStatus');
        Route::post('update-seller-status', 'AdminController@updateSellerStatus');
    });
});