$(document).ready(function () {
    // Chceck admin Current password
    $("#current_pwd").keyup(function () {
        var current_pwd = $("#current_pwd").val();
        $.ajax({
            type: "post",
            url: "/admin/check-current-pwd",
            data: {
                current_pwd: current_pwd
            },
            success: function (res) {
                if (res === "false") {
                    $("#chkCurrentPwd").html(
                        "<font color=red>Current Password is incorrect</font>"
                    );
                } else if (res === "true") {
                    $("#chkCurrentPwd").html(
                        "<font color=green>Current Password is Correct</font>"
                    );
                }
            },
            error: function (err) {
                alert(err);
            }
        });
    });
    // section status update
    $(".updateSectionStatus").click(function () {
        var status = $(this).text();
        var section_id = $(this).attr("section_id");
        $.ajax({
            type: "post",
            url: "/admin/update-section-status",
            data: {
                status: status,
                section_id: section_id
            },
            success: function (res) {
                if (res["status"] == 0) {
                    $("#section-" + section_id).html(
                        '<a class="updateSectionStatus" href="javascript:void(0)" id="' +
                        section_id +
                        '" section_id=""' +
                        section_id +
                        '"">Inactive</a>'
                    );
                } else {
                    $("#section-" + section_id).html(
                        '<a class="updateSectionStatus" href="javascript:void(0)" id="' +
                        section_id +
                        '" section_id=""' +
                        section_id +
                        '"">Active</a>'
                    );
                }
            }
        });
    });
    // category status update
    $(".updateCategoryStatus").click(function () {
        var status = $(this).text();
        var category_id = $(this).attr("category_id");
        // console.log(status);
        // console.log(category_id);

        $.ajax({
            type: "post",
            url: "/admin/update-category-status",
            data: {
                status: status,
                category_id: category_id
            },
            success: function (res) {
                // alert(res["status"]);
                if (res["status"] == 0) {
                    $("#category-" + category_id).html(
                        '<a class="updateCategoryStatus" href="javascript:void(0)" id="' +
                        category_id +
                        '" category_id=""' +
                        category_id +
                        '"">Inactive</a>'
                    );
                } else {
                    $("#category-" + category_id).html(
                        '<a class="updateCategoryStatus" href="javascript:void(0)" id="' +
                        category_id +
                        '" category_id=""' +
                        category_id +
                        '"">Active</a>'
                    );
                }
            }
        });
    });
    // product status update
    $(".updateProductStatus").click(function () {
        var status = $(this).text();
        var product_id = $(this).attr("product_id");
        $.ajax({
            type: "post",
            url: "/admin/update-product-status",
            data: {
                status: status,
                product_id: product_id
            },
            success: function (res) {
                if (res["status"] == 0) {
                    $("#product-" + product_id).html(
                        '<a class="updateProductStatus" href="javascript:void(0)" id="' +
                        product_id +
                        '" product_id=""' +
                        product_id +
                        '"">Inactive</a>'
                    );
                } else {
                    $("#product-" + product_id).html(
                        '<a class="updateProductStatus" href="javascript:void(0)" id="' +
                        product_id +
                        '" product_id=""' +
                        product_id +
                        '"">Active</a>'
                    );
                }
            }
        });
    });

    // Section dom render
    $("#section_id").change(function () {
        var section_id = $(this).val();

        $.ajax({
            type: "post",
            url: "/admin/append-categories-level",
            data: {
                section_id: section_id,
                _token: $("#token").val()
            },
            success: function (res) {
                // alert(res);
                $("#appendCategoriesLevel").html(res);
            },
            error: function (err) {
                alert(err);
            }
        });
    });
    $("#owner_section_id").change(function () {
        var section_id = $(this).val();

        $.ajax({
            type: "post",
            url: "/owner/append-categories-level",
            data: {
                section_id: section_id,
                _token: $("#token").val()
            },
            success: function (res) {
                // alert(res);
                $("#appendCategoriesLevel").html(res);
            },
            error: function (err) {
                alert(err);
            }
        });
    });
    $(".confirmDelete").click(function () {
        var name = $(this).attr("name");
        if (confirm("Are You Sure To Delete This" + name + "?")) {
            return true;
        } else {
            return false;
        }
    });
    // Multiple field validation
    $(document).ready(function () {
        var maxField = 10; //Input fields increment limitation
        var addButton = $(".add_button"); //Add button selector
        var wrapper = $("#dynamicTable"); //Input field wrapper
        var fieldHTML =
            '<tr><td><input type="text" class="form-control" name="product_sku[]" id="product_sku" placeholder="SKU"></td><td><input type="text" class="form-control" name="product_size[]" id="product_size" placeholder="Size"></td><td><input type="text" class="form-control" name="product_price[]" id="product_price" placeholder="Price"></td><td><input type="text" class="form-control" name="product_stock[]" id="product_stock" placeholder="Stock"></td><td><li class="list-inline-item"><a href="javascript:void(0);" class="remove_button confirmDelete btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></td></tr>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function () {
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on("click", ".remove_button", function (e) {
            e.preventDefault();
            $(this)
                .closest("tr")
                .remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
    $('.order_status').change(function () {
        var status_value = $(this).val();
        var order_id = $(this).data('id');
        $.ajax({
            type: 'post',
            url: "/order/change-status",
            data: {
                "status_value": status_value,
                "order_id": order_id
            },
            success: function (res) {
                if (res == 'success') {
                    window.location.reload();

                }
            },
            error: function (err) {
                alert(err);
            }
        })
    });
    // section status update
    $(".updateOwnerStatus").click(function () {
        var status = $(this).text();
        var owner_id = $(this).attr("owner_id");
        $.ajax({
            type: "post",
            url: "/admin/update-owner-status",
            data: {
                status: status,
                owner_id: owner_id
            },
            success: function (res) {
                if (res["status"] == 0) {
                    $("#owner_" + owner_id).html(
                        '<a class="updateOwnerStatus" href="javascript:void(0)" id="' +
                        owner_id +
                        '" owner_id=""' +
                        owner_id +
                        '"">Inactive</a>'
                    );
                } else {
                    $("#owner_" + owner_id).html(
                        '<a class="updateOwnerStatus" href="javascript:void(0)" id="' +
                        owner_id +
                        '" owner_id=""' +
                        owner_id +
                        '"">Active</a>'
                    );
                }
            }
        });
    });

    $(".updateSellerStatus").click(function () {
        var status = $(this).text();
        var seller_id = $(this).attr("seller_id");
        $.ajax({
            type: "post",
            url: "/admin/update-seller-status",
            data: {
                status: status,
                seller_id: seller_id
            },
            success: function (res) {
                if (res["status"] == 0) {
                    $("#seller_" + seller_id).html(
                        '<a class="updateSellerStatus" href="javascript:void(0)" id="' +
                        seller_id +
                        '" owner_id=""' +
                        seller_id +
                        '"">Inactive</a>'
                    );
                } else {
                    $("#seller_" + seller_id).html(
                        '<a class="updateSellerStatus" href="javascript:void(0)" id="' +
                        seller_id +
                        '" seller_id=""' +
                        seller_id +
                        '"">Active</a>'
                    );
                }
            }
        });
    });
});
