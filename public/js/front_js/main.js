/*price range*/

$("#sl2").slider();

var RGBChange = function () {
    $("#RGB").css(
        "background",
        "rgb(" + r.getValue() + "," + g.getValue() + "," + b.getValue() + ")"
    );
};

/*scroll to top*/

$(document).ready(function () {
    $(function () {
        $.scrollUp({
            scrollName: "scrollUp", // Element ID
            scrollDistance: 300, // Distance from top/bottom before showing element (px)
            scrollFrom: "top", // 'top' or 'bottom'
            scrollSpeed: 300, // Speed back to top (ms)
            easingType: "linear", // Scroll to top easing (see http://easings.net/)
            animation: "fade", // Fade, slide, none
            animationSpeed: 200, // Animation in speed (ms)
            scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
            //scrollTarget: false, // Set a custom target element for scrolling to the top
            scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
            scrollTitle: false, // Set a custom <a> title if required.
            scrollImg: false, // Set true to use image
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 2147483647 // Z-Index for the overlay
        });
    });
});
$(document).ready(function () {
    $("#productSize").change(function () {
        var sizeId = $(this).val();
        if (sizeId == "") {
            return false;
        }
        $.ajax({
            type: "get",
            url: "/get-product-price",
            data: {
                sizeId: sizeId
            },
            success: function (res) {
                var arr = res.split("#");
                $('#product_price').val(arr[0]);
                $("#getPrice").html("TK " + arr[0]);
                if (arr[1] > 0) {
                    $("#productAvailability").html(
                        "<b>Availability: </b>In stock"
                    );
                } else {
                    $("#productAvailability").html(
                        "<b>Availability: </b>Out Of Stock"
                    );
                }
            },
            error: function (err) {
                alert(err);
            }
        });
    });
    $(".changeImage").click(function () {
        var img = $(this).attr("src");
        $(".mainImage").attr("src", img);
    });
    // Instantiate EasyZoom instances
    var $easyzoom = $(".easyzoom").easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter(".easyzoom--with-thumbnails").data("easyZoom");

    $(".thumbnails").on("click", "a", function (e) {
        var $this = $(this);

        e.preventDefault();

        // Use EasyZoom's `swap` method
        api1.swap($this.data("standard"), $this.attr("href"));
    });
    // Setup toggles example
    var api2 = $easyzoom.filter(".easyzoom--with-toggle").data("easyZoom");

    $(".toggle").on("click", function () {
        var $this = $(this);

        if ($this.data("active") === true) {
            $this.text("Switch on").data("active", false);
            api2.teardown();
        } else {
            $this.text("Switch off").data("active", true);
            api2._init();
        }
    });
    $('.cart_quantity_down').click(function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        var product_id = $(this).data('product_id');
        var quantity = -1;
        var size = $(this).data('size');

        $.ajax({
            type: 'post',
            url: '/update-cart',
            data: {
                id: id,
                quantity: quantity,
                product_id: product_id,
                size: size
            },
            success: function (res) {
                var arr = res.split("#");
                $('#cart_' + arr[0]).val(arr[1]);
                if (arr[1] > 1) {
                    $("#cart_quantity_down_" + arr[0]).css("display", "block");
                } else {
                    $("#cart_quantity_down_" + arr[0]).css("display", "none");
                }
                if (arr[1] > 0) {
                    $("#cart_total_price_" + arr[0]).html("TK " + arr[2]);
                    $('#total_price').html('TK ' + (parseInt(arr[3]) + parseInt(100)));
                }
            },
            error: function (err) {
                alert(err);

            }

        })
    })
    $('.cart_quantity_up').click(function () {
        event.preventDefault();
        var id = $(this).data('id');
        var quantity = 1;
        var product_id = $(this).data('product_id');
        var size = $(this).data('size');

        $.ajax({
            type: 'post',
            url: '/update-cart',
            data: {
                id: id,
                quantity: quantity,
                product_id: product_id,
                size: size
            },
            success: function (res) {
                var arr = res.split("#");
                $('#cart_' + arr[0]).val(arr[1]);
                if (arr[1] > 1) {
                    $("#cart_quantity_down_" + arr[0]).css("display", "block");
                } else {
                    $("#cart_quantity_down_" + arr[0]).css("display", "none");
                }
                if (arr[1] > 0) {
                    $("#cart_total_price_" + arr[0]).html("TK " + arr[2])
                    $('#total_price').html('TK ' + (parseInt(arr[3]) + parseInt(100)));
                }
            },
            error: function (err) {
                alert(err);

            }

        })
    })
    $("#same_address").click(function () {


        if ($("input[type=checkbox]").prop('checked') == true) {
            $('#shipping_name').val($('#user_name').val());
            $('#shipping_email').val($('#user_email').val());
            $('#shipping_address').val($('#user_address').val());
            $('#shipping_phone').val($('#user_phone').val());
            $('#shipping_state').val($('#user_state').val());
            $('#shipping_country').val($('#user_country').val());
            $('#shipping_zip').val($('#user_zip').val());

        } else {
            $('#shipping_name').val('');
            $('#shipping_email').val('');
            $('#shipping_address').val('');
            $('#shipping_phone').val('');
            $('#shipping_state').val('');
            $('#shipping_country').val('');
            $('#shipping_zip').val('');
        }

    });
    $("#pass").blur(function () {
        var password = $(this).val();
        $.ajax({
            type: 'post',
            url: "/account/check-pass",
            data: {
                "password": password
            },
            success: function (res) {
                if (res == 'match') {
                    $('#errMsg').removeClass('d-none');
                    $('#errMsg').addClass('green');
                    $('#errMsg').html('Password correct');
                } else {
                    $('#errMsg').removeClass('d-none');
                    $('#errMsg').addClass('red');
                    $('#errMsg').html('Password incorrect');
                }
            },
            error: function (err) {
                alert(err);
            }
        })
    });
});
