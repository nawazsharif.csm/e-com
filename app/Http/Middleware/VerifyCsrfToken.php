<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "/admin/append-categories-level",
        "/owner/append-categories-level",
        "/admin/check-current-pwd",
        "/admin/update-section-status",
        "/admin/update-category-status",
        "/get-product-price",
        "/update-cart",
        "/admin/update-product-status",
        "account/check-pass",
        "/order/change-status",
        "/admin/update-owner-status",
        "/admin/update-seller-status"
    ];
}