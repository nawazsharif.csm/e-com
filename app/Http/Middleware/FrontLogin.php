<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FrontLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->put('previous_url', $request->url());
        if (Auth::check()) {
            if (!empty($request->session()->has('forntSession'))) {
                return $next($request);
            }
        }
        return redirect('/user-login');
    }
}