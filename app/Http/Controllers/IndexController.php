<?php

namespace App\Http\Controllers;

use App\Category;
use App\Section;
use App\Product;
use App\Cart;
use App\User;
use App\Admin;
use App\Order;
use App\ProductsAttribute;
use App\ProductsImage;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Redirect;
use Image;
use Illuminate\Support\Facades\Mail;

use function GuzzleHttp\json_decode;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        // get All categories with subcategories
        $productsAll = Product::inRandomOrder()->limit(6)->get();
        $categories = Category::with('categories')->where(['Parent_id' => 0])->get();
        $hotProducts = Product::where(['isHot' => 1, 'status' => 1])->inRandomOrder()->limit(20)->get();
        $categories = json_decode(json_encode($categories), true);
        // print_r('<pre>');
        // print_r($categories);
        // die;
        return view('index', compact('productsAll', 'categories', 'hotProducts'));
    }
    public function showProducts(Request $request, $url = null)
    {
        $countCategory = Category::where(['url' => $url])->count();
        if ($countCategory == 0) {
            return view('pages.404');
        }


        $hotProducts = Product::where(['isHot' => 1, 'status' => 1])->inRandomOrder()->limit(20)->get();
        $categories = Category::with('categories')->where(['Parent_id' => 0])->get();
        $categories = json_decode(json_encode($categories), true);
        $categoryDetails = Category::where(['url' => $url])->first();
        if ($categoryDetails->parent_id == 0) {
            $cat_ids = [$categoryDetails['id']];
            $subCategories = Category::where(['parent_id' => $categoryDetails->id])->get();
            foreach ($subCategories as $subCategory) {
                array_push($cat_ids, json_decode(json_encode($subCategory), true));
            }
            $productsAll = Product::whereIn('category_id', $cat_ids)->where(['status' => 1])->get();
            $parent = "";
        } else {
            $parent = Category::with(['categories', 'parentcategory'])->where(['url' => $url])->first();
            $parent = $parent->parentcategory->url;
            $productsAll = Product::where(['category_id' => $categoryDetails['id']])->get();
        }

        // print_r('<pre>');
        // print_r($categoryDetails->category_name);
        // print_r(json_decode(json_encode($categoryDetails), true));
        // die;
        return view('product.listing', compact('categoryDetails', 'productsAll', 'categories', 'parent', 'hotProducts'));
    }

    public function productsDetails($id)
    {
        $productAltImages = ProductsImage::where('product_id', $id)->get();
        $productAltImages = json_decode(json_encode($productAltImages), true);
        $categories = Category::with('categories')->where(['Parent_id' => 0])->get();
        $categories = json_decode(json_encode($categories), true);
        if (!empty($id)) {
            $productData = Product::with('attributes')->where(['id' => $id])->first();
        }
        //Related Product 
        $relatedProducts = Product::where('id', '!=', $id)->where(['category_id' => $productData->category_id])->get();
        // total Stock
        $totalProduct = ProductsAttribute::where('product_id', $id)->sum('stock');
        // print_r('<pre>');
        // print_r($productAltImages);
        // die;
        return view('product.details', compact('productData', 'categories', 'productAltImages', 'totalProduct', 'relatedProducts'));
    }
    public function getProductPrice(Request $request)
    {
        // echo $request->sizeId;
        if (isset($request->sizeId)) {
            $proArr = explode("-", $request->sizeId);
            $proAttr = ProductsAttribute::where(['product_id' => $proArr[0], 'size' => $proArr[1]])->first();
            // $proAttr = json_decode(json_encode($proAttr), true);
            // print_r($proAttr);
            echo $proAttr->price;
            echo "#";
            echo $proAttr->stock;
        }
    }
    public function addCart(Request $request)
    {
        $data = $request->all();
        if (empty($data['user_email'])) {
            $data['user_email'] = "";
        }
        $session_id = $request->session()->get('session_id', '');
        if (empty($session_id)) {
            $session_id = Str::random(40);
            $request->session()->put('session_id', $session_id);
        }
        $sizeArr = explode("-", $data['size']);
        $checkCart = Cart::where(['session_id' => $session_id, 'product_id' => $data['product_id']])->first();

        if (!empty($checkCart) && $checkCart->size == $sizeArr[1]) {
            Cart::where(['session_id' => $session_id, 'product_id' => $data['product_id']])
                ->update(
                    [
                        'product_name' => $data['product_name'],
                        'product_code' => $data['product_code'],
                        'product_color' => $data['product_color'],
                        'price' => $data['price'],
                        'user_email' => $data['user_email'],
                        'quantity' => $data['product_quantity'] + $checkCart['quantity']
                    ]
                );
        } else {
            DB::table('carts')
                ->insert(
                    [
                        'product_id' => $data['product_id'],
                        'owner_id' => $data['owner_id'],
                        'product_name' => $data['product_name'],
                        'product_code' => $data['product_code'],
                        'product_color' => $data['product_color'],
                        'price' => $data['price'],
                        'size' => $sizeArr[1],
                        'session_id' => $session_id,
                        'user_email' => $data['user_email'],
                        'quantity' => $data['product_quantity']
                    ]
                );
        }
        return redirect('cart')->with('success_message', 'Product Has been added in Cart!!');
    }
    public function cart(Request $request)
    {
        $session_id = $request->session()->get('session_id');
        $cart_lists = Cart::where('session_id', $session_id)->get();
        $total_price = 0;
        foreach ($cart_lists as $cart) {
            $total_price += $cart->price * $cart->quantity;
        }
        // return response()->json($cart_lists);

        return view('pages.cart')->with(compact('cart_lists', 'total_price'));
    }
    public function deleteCart(Request $request, $id)
    {
        if (!empty($id)) {
            Cart::find($id)->delete();
            return redirect('cart')->with('success_message', 'Product remove from cart Successfully');
        }
        return redirect('cart')->with('success_message', 'Np product found in the cart');
    }
    public function updateCart(Request $request)
    {
        $status = 0;
        $total_price = 0;

        $data = $request->all();
        $getQuantity = Cart::find($data['id']);
        $getSKU = ProductsAttribute::where(['product_id' => $data['product_id'] . '-' . $data['size'], 'size' => $data['size']])->first();
        $updateQuantity = $getQuantity->quantity + $data['quantity'];
        if ($updateQuantity > 0) {
            $updatePrice = 0;
            if ($getSKU->stock >= $updateQuantity) {
                Cart::where('id', $data['id'])
                    ->increment(
                        'quantity',
                        $data['quantity']
                    );
                $status = 1;
                $updatePrice = $updateQuantity * $getSKU->price;
            }
        }

        $session_id = $request->session()->get('session_id', '');
        $allCarts = Cart::where('session_id', $session_id)->get();
        foreach ($allCarts as $cart) {
            $total_price += $cart->price * $cart->quantity;
        }

        $value = Cart::find($data['id']);
        echo $data['id'];
        echo '#';
        echo $value->quantity;
        echo '#';
        echo $updatePrice;
        echo '#';
        echo $total_price;
        echo '#';
        echo $status;
    }

    public function checkout(Request $request)
    {
        $session_id = $request->session()->get('session_id');
        $cart_lists = Cart::where('session_id', $session_id)->get();
        $total_price = 0;
        foreach ($cart_lists as $cart) {
            $total_price += $cart->price * $cart->quantity;
            Cart::where(['id' => $cart->id])->update(['status' => 1, 'seller_id' => Auth::user()->id]);
        }

        if ($request->isMethod('post')) {
            $data = $request->all();
            $order = new Order;
            $order->user_id = $data['user_id'];
            $order->shipping_name = $data['shipping_name'];
            $order->shipping_email = $data['shipping_email'];
            $order->shipping_address = $data['shipping_address'];
            $order->shipping_state = $data['shipping_state'];
            $order->shipping_zip = $data['shipping_zip'];
            $order->shipping_phone = $data['shipping_phone'];
            $order->shipping_country = $data['shipping_country'];
            $order->total_price = $data['total_price'];
            $order->cart_id = $session_id;
            $order->save();
            $request->session()->forget('session_id');
            return redirect('/');

            // foreach($cart_lists )
        } else {
            if ($cart_lists->count() > 0) {
                return view('pages.checkout', compact('cart_lists', 'total_price'));
            }
        }
        return redirect('cart');
    }
    public function account(Request $request)
    {
        return view('pages.account');
    }
    public function editProfile(Request $request)
    {
        if ($request->isMethod('post')) {
            $image_name = "";
            $data = $request->all();


            $rules = [
                'name' => 'required',
                'mobile' => 'required|numeric',
                'address' => 'required',
            ];
            $customMessage = [
                'name.required' => 'Required Admin Name',
                'phone.required' => 'Required Phone Number',
                'phone.numeric' => 'Phone Number Invalid',
                'address.required' => 'Address Required',
            ];
            $this->validate($request, $rules, $customMessage);


            if ($request->hasFile('profile_image')) {

                $image_temp = $request->file('profile_image');
                if ($image_temp->isValid()) {
                    $extension = $image_temp->getClientOriginalExtension();
                    $image_name = rand(11, 99999) . '.' . $extension;
                    $image_path = 'images/user_images/' . $image_name;
                    //upload image here
                    Image::make($image_temp)->save($image_path);
                } else if (!empty($request->profile_image)) {
                    $image_name = $request->current_image_name;
                } else {
                    $image_name = $data['prev_img'];
                }
            }

            $user = User::find(Auth::user()->id);
            $user->name = $data['name'];
            $user->mobile = $data['mobile'];
            $user->address = $data['address'];
            $user->state = $data['state'];
            $user->zip = $data['zip'];
            $user->country = $data['country'];
            $user->image = $image_name;
            $user->save();
            $request->session()->flash('success_message', 'Profile Updated Successful');
            return redirect('/account/profile');
        }
        return view('pages.edit_profile');
    }
    public function checkPass(Request $request)
    {
        $data = $request->all();

        $password = Hash::make($data['password']);

        if (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password])) {
            echo 'match';
        } else {
            echo "don't matched";
        }
    }
    public function resetPass(Request $request)
    {
        if ($request->new_pass === $request->re_pass) {
            if (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password])) {

                $user = User::find(Auth::user()->id);
                $user->password = Hash::make($request->new_pass);
                $user->save();
                $request->session()->flash('success_message', 'Password Updated Successful');
                return Redirect('/account/profile');
            } else {
                $request->session()->flash('error_message', 'Current password dose not match');
            }
        } else {
            $request->session()->flash('error_message', 'Password dose not match');
            return redirect()->back();
        }
    }
    public function allOrders(Request $request)
    {

        # code...
    }
    public function sendMailCustomer($template, $data, $subject, $from_admin = false)
    {
        $to = isset($data['to']) ? $data['to'] : [];
        $mail_form_name = isset($data['mail_form_name']) ? $data['mail_form_name'] : '';
        $from_email = isset($data['from_email']) ? $data['from_email'] : '';
        try {
            Mail::queue($template, $data, function ($message) use ($to, $subject, $from_admin, $mail_form_name, $from_email) {
                $message->from($from_email, $mail_form_name);
                $message->to($to)->subject($subject);
            });
            return ['error' => 0];
        } catch (\Exception $e) {
            dump($e->getMessage());
        }
    }
}