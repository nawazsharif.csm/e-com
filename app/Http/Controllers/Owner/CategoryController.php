<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function appendCategoriesLevel(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $getCategories = Category::with('subcategories')->where(['section_id' => $data['section_id'], 'parent_id' => 0, 'status' => 1])->get();
            $getCategories = json_decode(json_encode($getCategories), true);
            // print_r('<pre>');
            // print_r($getCategories);
            // die;
            if ($request->session()->get('page') == "categories") {
                return view('admin.category.append_categories_level', compact('getCategories'));
            } else {
                return view('admin.product.append_categories_level', compact('getCategories'));
            }
        }
    }
}