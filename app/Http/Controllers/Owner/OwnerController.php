<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Middleware\Owner;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Cart;
use DB;

class OwnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('owner');
    }
    /**
     * dashboard Controller Function
     */
    function dashboard(Request $request)
    {
        $request->session()->put('page', 'dashboard');
        return view('owner.owner_dashboard');
    }

    /**
     * logout controller function
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
    /**
     * settings controller function
     */
    public function settings(Request $request)
    {
        $request->session()->put('page', 'settings');
        if ($request->isMethod('post')) {
        }
        return view('owner.admin_setting');
    }
    /**
     * Check Current password
     */
    public function chkCurrentPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Hash::check($request->current_pwd, Auth::guard('admin')->user()->password)) {
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }
    /**
     * Update Current password
     */
    public function updateCurrentPassword(Request $request)
    {
        $request->session()->put('page', 'updateCurrentPassword');
        if ($request->isMethod('post')) {
            $data = $request->all();
            // current password is correct
            if (Hash::check($data['current_pwd'], Auth::guard('admin')->user()->password)) {
                // check if new and confirm password is matching

                if ($data['new_pwd'] == $data['confirm_pwd']) {
                    Admin::where('id', Auth::guard('admin')->user()->id)->update(['password' => bcrypt($data['new_pwd'])]);
                    $request->session()->flash('success_message', 'Password has been updated successfully');
                    return redirect()->back();
                } else {
                    $request->session()->flash('error_message', 'New Password And confirm Password not match');
                    return redirect()->back();
                }
            } else {
                $request->session()->flash('error_message', 'Current password is incorrect');
                return redirect()->back();
            }
        }
    }
    /**
     *  update Admin Details function 
     */
    public function updateOwnerDetails(Request $request)
    {
        $request->session()->put('page', 'updateAdminDetails');
        $admin_details = Auth::guard('admin')->user();

        if ($request->isMethod('post')) {
            $rules = [
                'name' => 'required',
                'phone' => 'required|numeric',
                'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];
            $customMessage = [
                'name.required' => 'Required Admin Name',
                'phone.required' => 'Required Phone Number',
                'phone.numeric' => 'Phone Number Invalid',
                'profile_image.image' => 'valid image required'
            ];
            $this->validate($request, $rules, $customMessage);
            if ($request->hasFile('profile_image')) {
                $image_temp = $request->file('profile_image');
                if ($image_temp->isValid()) {
                    $extension = $image_temp->getClientOriginalExtension();
                    $image_name = rand(11, 99999) . '.' . $extension;
                    $image_path = 'images/admin_photos/' . $image_name;
                    //upload image here
                    Image::make($image_temp)->save($image_path);
                } else if (!empty($request->profile_image)) {
                    $image_name = $request->current_image_name;
                } else {
                    $image_name = "";
                }
            }

            Admin::where('email', Auth::guard('admin')->user()->email)->update([
                'name' => $request->name,
                'mobile' => $request->phone,
                'image' => $image_name
            ]);
            $request->session()->flash('success_message', 'Admin Settings Updated');
            return redirect()->back();
        }
        return view('owner.owner_details', compact('admin_details'));
    }
    public function orders(Request $request)
    {
        $request->session()->put('page', 'order');
        $orders = DB::select('select session_id, sum(price) as totalAmount from carts where (owner_id = ' . Auth::guard('admin')->user()->id . ' and status != 0) group by session_id');

        // var_dump('<pre>');
        // var_dump(json_decode(json_encode($orders)));
        // var_dump($orders);
        // die;
        return view('owner.order.order', compact('orders'));
    }
    public function orderDetails(Request $request, $id)
    {
        $order_detail = Order::where(['cart_id' => $id])->first();
        $order_list = Cart::where(['session_id' => $id, 'owner_id' => Auth::guard('admin')->user()->id])->get();
        $total_amount = Cart::select(DB::raw(' sum(price) as totalAmount'))->where(['session_id' => $id, 'owner_id' => Auth::guard('admin')->user()->id])->get('totalAmount');
        $orders = Order::where(['cart_id' => $id])->first('created_at');

        return view('owner.order.order_details', compact('order_list', 'order_detail', 'orders', 'total_amount'));
    }
}