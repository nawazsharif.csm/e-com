<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Section;
use App\Product;
use App\Admin;
use App\ProductAttribute;
use App\ProductsAttribute;
use App\ProductsImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Session\Session;
use Image;

class ProductController extends Controller
{
    public function addEditProduct(Request $request, $id = null)
    {
        $request->session()->put('page', 'products');
        $user_id = Auth::guard('admin')->user()->id;
        $owners = Admin::where(['type' => 'owner', 'status' => 1])->get();
        if ($id == "") {
            // add Category Function
            $title = "Add Product";
            $product = new Product;
            $productData = array();
            $getCategories = array();
            $message = 'Category Inserted successfully';
            $rules = [
                'product_name' => 'required|unique:products',
                'section_id' => 'required',
                'parent_id' => 'required',
                'product_code' => 'required',
                'product_price' => 'required',
                'product_image' => 'image',
            ];
        } else {
            // edit category functions
            $title = "Edit Product";
            $productData = Product::where('id', $id)->first();

            $productData = json_decode(json_encode($productData), true);
            $getCategories = Category::with('subcategories')->where(['parent_id' => 0, 'section_id' => $productData['section_id']])->get();
            $getCategories = json_decode(json_encode($getCategories), true);
            $product = Product::find($id);
            $message = 'Category Update successful';

            $rules = [
                'product_name' => 'required',
                'section_id' => 'required',
                'parent_id' => 'required',
                'product_code' => 'required',
                'product_price' => 'required',
                'product_image' => 'image',
            ];
            // echo '<pre>';
            // var_dump($productData);
            // die;
        }
        // get all Sections
        $sections = Section::all();

        if ($request->isMethod('post')) {
            $owner_id = !empty($request->owner_id) ? $request->owner_id : $user_id;
            $data = $request->all();
            // Category Validations
            $customMessage = [
                'product_name.required' => 'Product Name Required',
                'product_name.unique'   => 'Product Name Already Inserted',
                'section_id.required'   => 'Section Id Required',
                'parent_id.required'    => 'Category Id Required',
                'product_price.required' => 'Product Price Required',
                'product_code.required' => 'Product Code Required',
                'product_image.image'   => 'Valid Image Required',
            ];
            $this->validate($request, $rules, $customMessage);

            //upload Category Image
            if ($request->hasFile('product_image')) {
                $image_temp = $request->file('product_image');
                if ($image_temp->isValid()) {
                    $extension = $image_temp->getClientOriginalExtension();
                    $image_name = rand(11, 99999) . '.' . $extension;
                    $image_path = 'images/product_images/' . $image_name;
                    //upload image here
                    Image::make($image_temp)->save($image_path);
                    // Save Category Image
                    $product->image = $image_name;
                }
            } else {
                $product->image = 'placeholder.png';
            }
            if (empty($data['product_color'])) {
                $data['product_color'] = "";
            }
            if (empty($data['description'])) {
                $data['description'] = "";
            }

            $product->category_id = $data['parent_id'];
            $product->created_by = $user_id;
            $product->product_owner = $owner_id;
            $product->section_id = $data['section_id'];
            $product->product_name = $data['product_name'];
            $product->product_code = $data['product_code'];
            $product->product_color = $data['product_color'];
            $product->description = $data['description'];
            $product->price = $data['product_price'];
            $product->status = 0;
            $product->save();
            //Success message 
            $request->session()->flash('success_message', $message);
            return redirect('owner/products');
        }

        return view('owner.product.add_edit_product')->with((compact('title', 'sections', 'productData', 'getCategories', 'owners')));
    }

    public function deleteProductImage(Request $request, $id)
    {
        $productImage = Product::select('image')->where('id', $id)->first('image');
        $productImage = json_decode(json_encode($productImage), true);
        // print_r($categoryImage['category_image']);
        // die;
        // get Category image path
        $product_image_path = 'images/category_images/';
        // var_dump(file_exists($category_image_path . $categoryImage['category_image']));
        // die;
        if ($productImage['image'] != 'placeholder.png') {
            if (file_exists($product_image_path . $productImage['image'])) {
                unlink($product_image_path . $productImage['image']);
            }
            // Delete Category image from database
            Product::where('id', $id)->update(['image' => '']);
        }
        return redirect()->back()->with('flash_success_message', 'Product Image Deleted');
    }
    public function deleteImage(Request $request, $id)
    {
        $Image = ProductsImage::select('image')->where('id', $id)->first();
        $Image = json_decode(json_encode($Image), true);
        // print_r($categoryImage['category_image']);
        // die;
        // get Category image path
        $product_image_path = 'images/product_images/products/';
        // var_dump(file_exists($category_image_path . $categoryImage['category_image']));
        // die;
        if (file_exists($product_image_path . 'large' . $Image['image'])) {
            unlink($product_image_path . 'large' . $Image['image']);
            unlink($product_image_path . 'medium' . $Image['image']);
            unlink($product_image_path . 'small' . $Image['image']);
        }
        // Delete Category image from database
        ProductsImage::where('id', $id)->delete();

        return redirect()->back()->with('flash_success_message', 'Image Deleted Successfully');
    }
    public function deleteProduct(Request $request, $id)
    {
        Product::where('id', $id)->delete();
        $message = "Product Has been Deleted Successfully";
        $request->session()->flash('success_message', $message);
        return redirect()->back();
    }
    public function deleteAttribute(Request $request, $id)
    {
        ProductsAttribute::where('id', $id)->delete();
        $message = "Attribute Has been Deleted Successfully";
        $request->session()->flash('flash_success_message', $message);
        return redirect()->back();
    }
    public function products(Request $request, $url = null)
    {
        $request->session()->put('page', 'products');
        $owner_id = Auth::guard('admin')->user()->id;
        $productData = Product::with(['category', 'section'])->where('product_owner', $owner_id)->get();
        return view('owner.product.product_list', compact('productData'));
    }

    public function addAttributes(Request $request, $id)
    {
        $productData = Product::with(['attributes', 'section'])->where(['id' => $id])->first();
        $productData = json_decode(json_encode($productData), true);

        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['product_sku'] as $key => $val) {
                if (!empty($val)) {
                    $attribute = new ProductsAttribute;
                    $attribute->product_id = $id;
                    $attribute->sku = $val;
                    $attribute->size = $data['product_size'][$key];
                    $attribute->price = $data['product_price'][$key];
                    $attribute->stock = $data['product_stock'][$key];
                    $attribute->save();
                }
            }
            return redirect('owner/add-attributes/' . $id)->with('flash_message_success', 'Product Attributes has been added Successfully!');
        }


        return view('owner.product.add_attributes')->with(compact('productData'));
    }
    public function addProductImages(Request $request, $id)
    {
        $request->session()->put('page', 'products');
        $productData = Product::with(['images'])->where(['id' => $id])->first();
        $productData = json_decode(json_encode($productData), true);
        if ($request->isMethod('post')) {
            $data = $request->all();
            if ($request->hasFile('product_image')) {
                $files = $request->file('product_image');
                foreach ($files as $file) {
                    // upload image after resize
                    $image = new ProductsImage;
                    $extension = $file->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/product_images/products/large' . $filename;
                    $medium_image_path = 'images/product_images/products/medium' . $filename;
                    $small_image_path = 'images/product_images/products/small' . $filename;
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(600, 300)->save($medium_image_path);
                    Image::make($file)->resize(300, 300)->save($small_image_path);
                    $image->image = $filename;
                    $image->product_id = $data['product_id'];
                    $image->save();
                }
                return redirect('/owner/add-images/' . $data['product_id'])->with('flash_message_success', 'Product Images has been added Successfully!');
            }
        }
        return view('admin.product.add_product_images')->with(compact('productData'));
    }
    public function appendCategoriesLevel(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $getCategories = Category::with('subcategories')->where(['section_id' => $data['section_id'], 'parent_id' => 0, 'status' => 1])->get();
            $getCategories = json_decode(json_encode($getCategories), true);
            if ($request->session()->get('page') == "categories") {
                return view('admin.category.append_categories_level', compact('getCategories'));
            } else {
                return view('owner.product.append_categories_level', compact('getCategories'));
            }
        }
    }
    public function activeProducts(Request $request)
    {
        $request->session()->put('page', 'activeProducts');
        $owner_id = Auth::guard('admin')->user()->id;
        $productData = Product::with(['category', 'section'])->where(['product_owner' => $owner_id, 'status' => 1])->get();
        return view('owner.product.product_list', compact('productData'));
    }
    public function inactiveProducts(Request $request)
    {
        $request->session()->put('page', 'inactiveProducts');
        $owner_id = Auth::guard('admin')->user()->id;
        $productData = Product::with(['category', 'section'])->where(['product_owner' => $owner_id, 'status' => 0])->get();
        return view('owner.product.product_list', compact('productData'));
    }
}