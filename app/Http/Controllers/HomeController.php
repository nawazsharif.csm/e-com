<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.admin_dashboard');
    }
    public function orderStatus(Request $request)
    {
        $data = $request->all();
        Cart::where(['id' => $data['order_id']])->update(['status' => $data['status_value']]);
        $request->session()->flash('success_message', 'Status update successful');
        echo 'success';
    }
}