<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;
use Image;
use App\Cart;
use App\User;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{

    /**
     * dashboard Controller Function
     */
    function dashboard(Request $request)
    {
        $request->session()->put('page', 'dashboard');
        return view('admin.admin_dashboard');
    }
    /**
     * Login controller function
     */
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();

            $rule =
                [
                    'email' => 'required|email|max:255',
                    'password' => 'required'
                ];
            $customMessage = [
                'email.required' => 'Email is required',
                'email.email' => 'Invalid Email',
                'password.required' => 'Password Required'
            ];
            $this->validate($request, $rule, $customMessage);
            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
                if (Auth::guard('admin')->user()->type === 'admin') {
                    return redirect('/admin/dashboard');
                } else {
                    return redirect('/owner/dashboard');
                }
            } else {
                $request->session()->flash('error_message', 'Invalid Email or Password');
                return redirect()->back();
            }
        }
        return view('admin.admin_login');
    }
    /**
     * logout controller function
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
    /**
     * settings controller function
     */
    public function settings(Request $request)
    {
        $request->session()->put('page', 'settings');
        if ($request->isMethod('post')) {
        }
        return view('admin.admin_setting');
    }
    /**
     * Check Current password
     */
    public function chkCurrentPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Hash::check($request->current_pwd, Auth::guard('admin')->user()->password)) {
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }
    /**
     * Update Current password
     */
    public function updateCurrentPassword(Request $request)
    {
        $request->session()->put('page', 'updateCurrentPassword');
        if ($request->isMethod('post')) {
            $data = $request->all();
            // current password is correct
            if (Hash::check($data['current_pwd'], Auth::guard('admin')->user()->password)) {
                // check if new and confirm password is matching

                if ($data['new_pwd'] == $data['confirm_pwd']) {
                    Admin::where('id', Auth::guard('admin')->user()->id)->update(['password' => bcrypt($data['new_pwd'])]);
                    $request->session()->flash('success_message', 'Password has been updated successfully');
                    return redirect()->back();
                } else {
                    $request->session()->flash('error_message', 'New Password And confirm Password not match');
                    return redirect()->back();
                }
            } else {
                $request->session()->flash('error_message', 'Current password is incorrect');
                return redirect()->back();
            }
        }
    }
    /**
     *  update Admin Details function 
     */
    public function updateAdminDetails(Request $request)
    {
        $request->session()->put('page', 'updateAdminDetails');
        $admin_details = Auth::guard('admin')->user();

        if ($request->isMethod('post')) {
            $rules = [
                'name' => 'required',
                'phone' => 'required|numeric',
                'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];
            $customMessage = [
                'name.required' => 'Required Admin Name',
                'phone.required' => 'Required Phone Number',
                'phone.numeric' => 'Phone Number Invalid',
                'profile_image.image' => 'valid image required'
            ];
            $this->validate($request, $rules, $customMessage);
            if ($request->hasFile('profile_image')) {
                $image_temp = $request->file('profile_image');
                if ($image_temp->isValid()) {
                    $extension = $image_temp->getClientOriginalExtension();
                    $image_name = rand(11, 99999) . '.' . $extension;
                    $image_path = 'images/admin_photos/' . $image_name;
                    //upload image here
                    Image::make($image_temp)->save($image_path);
                } else if (!empty($request->profile_image)) {
                    $image_name = $request->current_image_name;
                } else {
                    $image_name = "";
                }
            }

            Admin::where('email', Auth::guard('admin')->user()->email)->update([
                'name' => $request->name,
                'mobile' => $request->phone,
                'image' => $image_name
            ]);
            $request->session()->flash('success_message', 'Admin Settings Updated');
            return redirect()->back();
        }
        return view('admin.admin_details', compact('admin_details'));
    }
    public function orders(Request $request)
    {
        $request->session()->put('page', 'order');

        $orders = Order::with(['salesman', 'items'])->orderBy('id', 'DESC')->get();
        // var_dump('<pre>');
        // var_dump(json_decode(json_encode($orders)));
        // var_dump($orders);
        // die;
        return view('admin.order.order', compact('orders'));
    }
    public function orderDetails(Request $request, $id)
    {
        $request->session()->put('page', 'order');
        $order_list = Cart::where(['session_id' => $id])->get();
        return view('admin.order.order_details', compact('order_list'));
    }
    public function allOwners(Request $request)
    {
        $request->session()->put('page', 'allOwner');
        $owners = Admin::where(['type' => 'owner'])->get();
        return view('admin.user.owner', compact('owners'));
    }
    public function inActiveOwners(Request $request)
    {
        $request->session()->put('page', 'allInactiveOwner');
        $owners = Admin::where(['type' => 'owner'])->where('status', '=!', 1)->get();
        return view('admin.user.owner', compact('owners'));
    }

    /**
     *  get all salesMan
     */
    public function allSellers(Request $request)
    {
        $request->session()->put('page', 'allSeller');
        $owners = User::where(['status' => 1])->get();
        return view('admin.user.seller', compact('owners'));
    }
    /**
     *  get all salesMan
     */
    public function inActiveSellers(Request $request)
    {
        $request->session()->put('page', 'allInactiveSeller');
        $owners = User::where('status', '=!', 1)->get();
        return view('admin.user.seller', compact('owners'));
    }
    public function updateOwnerStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Admin::where('id', $data['owner_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'owner_id' => $data['owner_id']]);
        }
    }
    public function updateSellerStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            User::where('id', $data['seller_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'seller_id' => $data['seller_id']]);
        }
    }
}