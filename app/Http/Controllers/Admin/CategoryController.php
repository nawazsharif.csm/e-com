<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Section;
use Image;

use function GuzzleHttp\json_decode;

class CategoryController extends Controller
{
    public function categories(Request $request)
    {
        $request->session()->put('page', 'categories');
        $categories = Category::with(['section', 'parentcategory'])->get();
        // echo '<pre>';
        // var_dump($categories->toArray();
        // die;
        return view('admin.category.categories')->with(compact('categories'));
    }
    public function updateCategoryStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();

            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Category::where('id', $data['category_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'category_id' => $data['category_id']]);
        }
    }
    public function addEditCategory(Request $request, $id = null)
    {

        if ($id == "") {
            // add Category Function
            $title = "Add Category";
            $category = new Category;
            $categorydata = array();
            $getCategories = array();
            $message = 'Category Inserted successfully';
        } else {
            // edit category functions
            $title = "Edit Category";
            $categorydata = Category::where('id', $id)->first();
            $categorydata = json_decode(json_encode($categorydata), true);
            $getCategories = Category::with('subcategories')->where(['parent_id' => 0, 'section_id' => $categorydata['section_id']])->get();
            $getCategories = json_decode(json_encode($getCategories), true);
            $category = Category::find($id);
            $message = 'Category Update successful';
            // echo '<pre>';
            // var_dump($getCategories);
        }
        // get all Sections
        $sections = Section::all();

        if ($request->isMethod('post')) {
            $data = $request->all();
            // Category Validations
            $rules = [
                'category_name' => 'required',
                'section_id' => 'required',
                'url' => 'required',
                'category_image' => 'image',
            ];
            $customMessage = [
                'category_name.required' => 'Category Name Required',
                // 'category_name.unique' => 'Category Name Already Inserted',
                'section_id.required' => 'Section Id Required',
                'url.required' => 'Category URL Required',
                'category_image.image' => 'Valid Image Required',
            ];
            $this->validate($request, $rules, $customMessage);
            //upload Category Image
            if ($request->hasFile('category_image')) {
                $image_temp = $request->file('category_image');
                if ($image_temp->isValid()) {
                    $extension = $image_temp->getClientOriginalExtension();
                    $image_name = rand(11, 99999) . '.' . $extension;
                    $image_path = 'images/category_images/' . $image_name;
                    //upload image here
                    Image::make($image_temp)->save($image_path);
                    // Save Category Image
                    $category->category_image = $image_name;
                }
            } else {
                $category->category_image = 'placeholder.png';
            }
            if (empty($data['category_discount'])) {
                $data['category_discount'] = 0.0;
            }
            if (empty($data['meta_title'])) {
                $data['meta_title'] = "";
            }
            if (empty($data['description'])) {
                $data['description'] = "";
            }
            if (empty($data['meta_description'])) {
                $data['meta_description'] = "";
            }
            if (empty($data['meta_keyword'])) {
                $data['meta_keyword'] = "";
            }
            if (empty($data['section_id'])) {
                $data['section_id'] = 0;
            }

            $category->parent_id = $data['parent_id'];
            $category->section_id = $data['section_id'];
            $category->category_name = $data['category_name'];
            // $category->category_image = $data['category_image'];
            $category->category_discount = $data['category_discount'];
            $category->description = $data['description'];
            $category->url = $data['url'];
            $category->meta_title = $data['meta_title'];
            $category->meta_description = $data['meta_description'];
            $category->meta_keyword = $data['meta_keyword'];
            $category->status = 1;
            $category->save();
            //Success message 
            $request->session()->flash('success_message', $message);
            return redirect('admin/categories');
        }

        return view('admin.category.add_edit_category')->with((compact('title', 'sections', 'categorydata', 'getCategories')));
    }
    public function appendCategoriesLevel(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $getCategories = Category::with('subcategories')->where(['section_id' => $data['section_id'], 'parent_id' => 0, 'status' => 1])->get();
            $getCategories = json_decode(json_encode($getCategories), true);
            // print_r('<pre>');
            // print_r($getCategories);
            // die;
            if ($request->session()->get('page') == "categories") {
                return view('admin.category.append_categories_level', compact('getCategories'));
            } else {
                return view('admin.product.append_categories_level', compact('getCategories'));
            }
        }
    }
    public function deleteCategoryImage(Request $request, $id)
    {
        $categoryImage = Category::select('category_image')->where('id', $id)->first('category_image');
        $categoryImage = json_decode(json_encode($categoryImage), true);
        // print_r($categoryImage['category_image']);
        // die;
        // get Category image path
        $category_image_path = 'images/category_images/';
        // var_dump(file_exists($category_image_path . $categoryImage['category_image']));
        // die;
        if (file_exists($category_image_path . $categoryImage['category_image'])) {
            unlink($category_image_path . $categoryImage['category_image']);
        }
        // Delete Category image from database
        Category::where('id', $id)->update(['category_image' => '']);

        return redirect()->back()->with('flash_success_message', 'Category Image Deleted');
    }
    public function deleteCategory(Request $request, $id)
    {
        Category::where('id', $id)->delete();
        $message = "Category Has been Deleted Successfully";
        $request->session()->flash('success_message', $message);
        return redirect()->back();
    }
}
