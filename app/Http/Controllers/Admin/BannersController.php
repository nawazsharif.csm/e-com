<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session\Session;
use Image;

class BannersController extends Controller
{
    public function addBanner(Request $request, $id = null)
    {
        $request->session()->put('page', 'banners');
        if ($id == "") {
            // add Banner Function
            $title = "Add Banner";
            $banner = new Banner;
            $bannerData = [];
            $message = 'Banner Inserted successfully';
            $rules = [
                'title' => 'required',
                'link' => 'required',
                'image' => 'image',
            ];
        } else {
            $title = "Edit Banner";
            $bannerData = Banner::where(['id' => $id])->first();
            $banner = Banner::find($id);

            $message = 'Banner Updated successfully';
            $rules = [
                'title' => 'required',
                'link' => 'required'
            ];
        }
        if ($request->isMethod('post')) {
            $data = $request->all();
            $customMessage = [
                'title.required' => 'Banner Title Required',
                'link.required' => 'Banner Link Required',
                'status.required' => 'Banner Status Required',
                'image.image'   => 'Valid Image Required',
            ];
            $this->validate($request, $rules, $customMessage);

            //upload Category Image
            if ($request->hasFile('image')) {
                $image_temp = $request->file('image');
                if ($image_temp->isValid()) {
                    $extension = $image_temp->getClientOriginalExtension();
                    $image_name = rand(11, 99999) . '.' . $extension;
                    $image_path = 'images/banner_images/' . $image_name;
                    //upload image here
                    Image::make($image_temp)->save($image_path);
                    // Save Category Image
                    $banner->image = $image_name;
                }
            } else {
                $banner->image = $data['prev_img'];
            }
            if (empty($data['status'])) {
                $data['status'] = 0;
            }
            $banner->title = $data['title'];
            $banner->link = $data['link'];
            $banner->status = $data['status'];
            $banner->save();
            $request->session()->flash('success_message', $message);
            return redirect('admin/banners');
        }
        return view('admin.banners.add_banner', compact('title', 'bannerData'));
    }
    public function bannerList(Request $request)
    {
        $request->session()->put('page', 'banners');
        $banners = Banner::all();
        return view('admin.banners.banners', compact('banners'));
    }
    public function deleteBanner(Request $request, $id)
    {
        Banner::where('id', $id)->delete();
        $message = "Banner Has been Deleted Successfully";
        $request->session()->flash('success_message', $message);
        return redirect()->back();
    }
    public function deleteBannerImage(Request $request, $id)
    {
        $bannerImage = Banner::select('image')->where('id', $id)->first('image');
        $bannerImage = json_decode(json_encode($bannerImage), true);
        // print_r($categoryImage['category_image']);
        // die;
        // get Category image path
        $product_image_path = 'images/category_images/';
        // var_dump(file_exists($category_image_path . $categoryImage['category_image']));
        // die;
        if ($bannerImage['image'] != 'placeholder.png') {
            if (file_exists($product_image_path . $bannerImage['image'])) {
                unlink($product_image_path . $bannerImage['image']);
            }
            // Delete Category image from database
            Banner::where('id', $id)->update(['image' => '']);
        }
        return redirect()->back()->with('success_message', 'Product Image Deleted');
    }
}