<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Admin;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    use AuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected $redirectTo = '/';

    public function redirectUrl(Request $request)
    {
        $previous_url = $request->session()->get('previous_url', '');
        if ($previous_url == 'http://127.0.0.1:8000/user-login') {
            return '/';
        } else {
            return $previous_url;
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $previous_url = $request->session()->get('previous_url', '');

            // if ($this->attemptLogin($request)) {
            //     return $this->sendLoginResponse($request);
            // }
            $input = $request->all();


            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];
            $customMessage = [
                'email.required' => 'Email is required',
                'email.email' => 'Invalid Email',
                'password.required' => 'Password Required'
            ];
            $this->validate($request, $rules, $customMessage);

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $request->session()->put('forntSession', $input['email']);

                if ($previous_url == '//' . $_SERVER['HTTP_HOST'] . 'user-login' || $previous_url == '') {
                    return redirect('/');
                } else {

                    return redirect($previous_url);
                }
            } else {
                $request->session()->flash('error_message', 'Invalid Email or Password');
                return redirect('user-login');
            }
        } else {
            return view('pages.login_register');
        }
    }


    public function register(Request $request)
    {
        $previous_url = $request->session()->get('previous_url', '');
        if ($request->isMethod('post')) {

            if ($request->user_type == 'owner') {
                $owner = new Admin;
                $owner->name = $request->name;
                $owner->type = $request->user_type;
                $owner->mobile = $request->phone;
                $owner->email = $request->email;
                $owner->status = 0;
                $owner->password = Hash::make($request->password);
                $owner->save();
                $email = $request->email;
                $name = $request->name;
                $messageData = ['email' => $request->email, 'name' => $request->name];
                // Mail::send('email.email', $messageData, function ($message) use ($email, $name) {
                //     $message->from('salesmanbd007@gmail.com', 'Admin');
                //     $message->sender('salesmanbd007@gmail.com', 'Admin');
                //     $message->to($email, $name);
                //     $message->cc('nawazsharif.csm@gmail.com', 'Nawaz Sharif');
                //     //     $message->bcc('john@johndoe.com', 'John Doe');
                //     $message->replyTo('salesmanbd007@gmail.com', 'Admin');
                //     $message->subject('Registration with SalesManBD As a owner');
                //     //     $message->priority(3);
                //     //     $message->attach('pathToFile');
                // });
                return redirect('/owner/dashboard');
            } else {
                $user = new User;
                $user->name = $request->name;
                $user->type = $request->user_type;
                $user->mobile = $request->phone;
                $user->email = $request->email;
                $user->status = 0;
                $user->password = Hash::make($request->password);
                $user->save();
                $email = (string) ($request->email);
                $name = (string) $request->name;
                $messageData = ['email' => $request->email, 'name' => $request->name];
                // Mail::later(5, 'email.email', $messageData, function ($message) use ($email, $name) {
                //     // $message->from('salesmanbd007@gmail.com', 'Admin');
                //     // $message->sender('salesmanbd007@gmail.com', 'Admin');
                //     $message->to($email, $name);
                //     // $message->cc('nawazsharif.csm@gmail.com', 'Nawaz Sharif');
                //     //     $message->bcc('john@johndoe.com', 'John Doe');
                //     // $message->replyTo('salesmanbd007@gmail.com', 'Admin');
                //     $message->subject('Registration with SalesManBD As a Seller');
                //     $message->priority(3);
                //     //     $message->attach('pathToFile');

                // });

                if (auth()->attempt(array('email' => $request->email, 'password' => $request->password))) {
                    $request->session()->put('forntSession', $request->email);
                    if ($previous_url == '//' . $_SERVER['HTTP_HOST'] . 'user-login' || $previous_url == '') {
                        return redirect('/');
                    } else {

                        return redirect($previous_url);
                    }
                }
            }
        }
        return view('pages.login_register');
    }
    public function logout(Request $request)
    {
        $request->session()->forget('forntSession');
        Auth::logout();
        return redirect('/');
    }
}