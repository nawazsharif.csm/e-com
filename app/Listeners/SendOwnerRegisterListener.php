<?php

namespace App\Listeners;

use App\Events\OwnerRegisterEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\mail\SendOwnerRegistrationEmail;
use Illuminate\Support\Facades\Mail;

class SendOwnerRegisterListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerRegisterEvent  $event
     * @return void
     */
    public function handle(OwnerRegisterEvent $event)
    {
        // Mail::to($event->owner)->send(new SendOwnerRegistrationEmail());
    }
    /**
     * Determine whether the listener should be queued.
     *
     * @param  \App\Events\OrderPlaced  $event
     * @return bool
     */
    public function shouldQueue(OwnerRegisterEvent $event)
    {
        // Mail::to($event->owner)->send(new SendOwnerRegistrationEmail());
        Mail::later(5, 'Html.view', $event->owner, function ($message) {
            $message->to('john@johndoe.com', 'John Doe');
            $message->cc('john@johndoe.com', 'John Doe');
            $message->bcc('john@johndoe.com', 'John Doe');
            $message->replyTo('john@johndoe.com', 'John Doe');
            $message->subject('Subject');
            $message->priority(3);
            $message->attach('pathToFile');
        });
    }
}
