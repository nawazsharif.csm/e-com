<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function salesman()
    {
        return $this->belongsTo('App\User', 'id')->select('id', 'name');
    }
    public function items()
    {
        return $this->hasMany('App\Cart', 'session_id','cart_id');
    }
}