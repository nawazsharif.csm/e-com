<?php

namespace App;

use App\Events\OwnerRegisterEvent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    protected $fillable = [
        'name', 'type', 'email', 'password', 'mobile', 'status', 'image', 'created_at', 'updated_at',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * Owner registration mail handel event
     */
    protected $dispatchesEvents = [
        'created' => OwnerRegisterEvent::class,
    ];
}
