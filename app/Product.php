<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function attributes()
    {
        return $this->hasMany('App\ProductsAttribute', 'product_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id')->select('id', 'category_name');
    }
    public function section()
    {
        return $this->belongsTo('App\Section', 'section_id')->select('id', 'name');
    }
    public function images()
    {
        return $this->hasMany('App\ProductsImage', 'product_id');
    }
    public function owner()
    {
        return $this->belongsTo('App\Admin', 'id')->select('id', 'name');
    }
}
