<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productRecoards = [
            ['id' => 1, 'section_id' => 1, 'category_id' => 1, 'product_name' => 'Half shirt', 'product_code' => 'HS01', 'product_color' => 'red', 'description' => '', 'price' => 100, 'image' => '', 'status' => 1],
            ['id' => 2, 'section_id' => 1, 'category_id' => 6, 'product_name' => 'Nike Shoe', 'product_code' => 'HS01', 'product_color' => 'red', 'description' => '', 'price' => 2000, 'image' => '', 'status' => 1],
        ];
        Product::insert($productRecoards);
    }
}