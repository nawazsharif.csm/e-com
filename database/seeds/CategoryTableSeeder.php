<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryRecoards = [
            ['id' => 1, 'parent_id' => 0, 'section_id' => 1, 'category_name' => 'T-Shirts', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 't-shirts', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 2, 'parent_id' => 1, 'section_id' => 1, 'category_name' => 'Casual T-Shirts', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'casual-t-shirts', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 3, 'parent_id' => 1, 'section_id' => 1, 'category_name' => 'Formal T-Shirts', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'formal-t-shirts', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 4, 'parent_id' => 0, 'section_id' => 1, 'category_name' => 'Shoes', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'shoes', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 5, 'parent_id' => 0, 'section_id' => 1, 'category_name' => 'Pants', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'pants', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 6, 'parent_id' => 4, 'section_id' => 1, 'category_name' => 'Formal Shoes', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'formal-shoes', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 7, 'parent_id' => 4, 'section_id' => 1, 'category_name' => 'Casual Shoes', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'casual-shoes', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
            ['id' => 8, 'parent_id' => 5, 'section_id' => 1, 'category_name' => 'Formal Pants', 'category_image' => '', 'category_discount' => 0, 'description' => '', 'url' => 'formal-pants', 'meta_title' => '', 'meta_description' => '', 'meta_keyword' => '', 'status' => 1],
        ];
        Category::insert($categoryRecoards);
    }
}
