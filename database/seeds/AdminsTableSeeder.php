<?php

use App\Http\Admin;
use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admins')->delete();
        $adminRecords = [
            ['id' => 1, 'name' => 'admin', 'type' => 'admin', 'mobile' => '0000000', 'email' => 'example@email.com', 'password' => '$2y$10$FELtriJUKfXeeGcxOzAmFeFTA//Rm6qsSO2m5CCDh90TiYey9ycBG', 'image' => '', 'status' => 1],
            ['id' => 2, 'name' => 'nawaz', 'type' => 'subadmin', 'mobile' => '0000000', 'email' => 'exampl1e@email.com', 'password' => '$2y$10$FELtriJUKfXeeGcxOzAmFeFTA//Rm6qsSO2m5CCDh90TiYey9ycBG', 'image' => '', 'status' => 1],
            ['id' => 3, 'name' => 'razu', 'type' => 'admin', 'mobile' => '0000000', 'email' => 'example2@email.com', 'password' => '$2y$10$FELtriJUKfXeeGcxOzAmFeFTA//Rm6qsSO2m5CCDh90TiYey9ycBG', 'image' => '', 'status' => 1],
            ['id' => 4, 'name' => 'kutta', 'type' => 'subadmin', 'mobile' => '0000000', 'email' => 'example3@email.com', 'password' => '$2y$10$FELtriJUKfXeeGcxOzAmFeFTA//Rm6qsSO2m5CCDh90TiYey9ycBG', 'image' => '', 'status' => 1],
        ];
        foreach ($adminRecords as $key => $record) {
            \App\Admin::create($record);
        }
    }
}
